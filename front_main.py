import pygame
from pygame.locals import *

pygame.init()

#Ouverture de la fenêtre Pygame
fenetre = pygame.display.set_mode((1530, 900),RESIZABLE)
taille = 10
vaisseau_taille_5 = pygame.image.load("super destroyer.png").convert_alpha()
vaisseau_taille_5 = pygame.transform.scale(vaisseau_taille_5,(500//taille*5-10,500//taille-10))
vaisseau_taille_4 = pygame.image.load("star_destroyer.png").convert()
vaisseau_taille_4.set_colorkey((0,0,0))
vaisseau_taille_4 = pygame.transform.scale(vaisseau_taille_4,(500//taille*4-10,500//taille-10))
vaisseau_taille_3 = pygame.image.load("corvette_cr90.png").convert_alpha()
vaisseau_taille_3 = pygame.transform.scale(vaisseau_taille_3,(500//taille*3-10,500//taille-10))
vaisseau_taille_3_bis = pygame.image.load("corvette_cr90.png").convert_alpha()
vaisseau_taille_3_bis = pygame.transform.scale(vaisseau_taille_3_bis,(500//taille*3-10,500//taille-10))
vaisseau_taille_2 = pygame.image.load("x_wing.png").convert_alpha()
vaisseau_taille_2 = pygame.transform.scale(vaisseau_taille_2,(500//taille*2-10,500//taille-10))
sad_trooper=pygame.image.load("sad_trooper.png").convert()
sad_trooper=pygame.transform.scale(sad_trooper, (500//taille-1, 500//taille-1))
explosion=pygame.image.load("explosion.png").convert_alpha()
explosion=pygame.transform.scale(explosion, (500//taille-1, 500// taille-1))

menu = 0 #vaut 0 pour le menu initial, 1 pour le 1er joueur, 2 pour le 2nd, 3 pour celui de transition
nature_action = 0 #0 pour la sélection d'un bâteau en placement, 1 pour un clic sur la grille pour le placement,
# 2 pour un clic sur la grille pour une attaque
bateaux_placés_j1 = [2,3,3,4,5]
bateaux_placés_j2 = [2,3,3,4,5]


def fond():
    '''
    Chargement et collage du fond
    '''
    fond_global = pygame.image.load("cockpit.jpg").convert()
    fond_global = pygame.transform.scale(fond_global, (1530,900))
    fenetre.blit(fond_global, (0,0))
    fond_grille = pygame.image.load("fond_espace.jpg").convert()
    fond_grille = pygame.transform.scale(fond_grille, (500//taille*(taille), 500//taille*(taille)))
    logo = pygame.image.load("Logo_La_Guérande.png").convert_alpha()
    logo = pygame.transform.scale(logo, (150,150))
    fenetre.blit(fond_grille, (150,70))
    fenetre.blit(fond_grille, (1350-500//taille*(taille-1),70))
    fenetre.blit(logo, (0,0))
    
def grille():
    '''
    Chargement et collage des grilles
    '''
    carre = pygame.image.load("oeuvre_d_art.png").convert()
    carre.set_colorkey((0,0,0))
    carre = pygame.transform.scale(carre, (500//taille,500//taille))
    for i in range (taille):
        for j in range (taille):
            fenetre.blit(carre, (500//taille*i+150,500//taille*j+70))
            fenetre.blit(carre, (1350-500//taille*i,500//taille*j+70))



def texte_principal(message):
    dico_text={0:"placez_vaisseau.png"}
    cadre_info = pygame.image.load("ecran.png").convert()
    cadre_info = pygame.transform.scale(cadre_info,(1300, 80))
    fenetre.blit(cadre_info, (120, 710) )
    texte= pygame.image.load(dico_text[message]).convert_alpha()
    texte = pygame.transform.scale(texte, (1200, 70))
    fenetre.blit(texte, (170,715))

def grille_de_gauche():
    '''
    Chargement et collage des icônes des vaisseaux côté gauche
    '''
    cadre_5 = pygame.image.load("ecran.png").convert()
    cadre_5.set_colorkey((4,51,61))
    cadre_5 = pygame.transform.scale(cadre_5,(500+35,500//taille*2+10))
    fenetre.blit(cadre_5,(150-25,600-10))
    fenetre.blit(vaisseau_taille_5,(150,600))
    fenetre.blit(vaisseau_taille_4,(150,600+ 500//taille))
    fenetre.blit(vaisseau_taille_3,(150+500-500//taille*3,600))
    fenetre.blit(vaisseau_taille_3_bis,(150+500-500//taille*3,600+500//taille))
    fenetre.blit(vaisseau_taille_2,(150+500//taille*4+500//(taille*2),600+500//taille))

def grille_de_droite():
    '''
    Chargement et collage des icônes des vaisseaux côté droit
    '''
    cadre_5 = pygame.image.load("ecran.png").convert()
    cadre_5.set_colorkey((4,51,61))
    cadre_5 = pygame.transform.scale(cadre_5,(500+35,500//taille*2+10))
    fenetre.blit(cadre_5,(1350-500//taille*(taille-1)-25,600-10))
    fenetre.blit(vaisseau_taille_5,(1350-500//taille*(taille-1),600))
    fenetre.blit(vaisseau_taille_4,(1350-500//taille*(taille-1),600+ 500//taille))
    fenetre.blit(vaisseau_taille_3,(1350-500//taille*(taille-1)+500-500//taille*3,600))
    fenetre.blit(vaisseau_taille_3_bis,(1350-500//taille*(taille-1)+500-500//taille*3,600+500//taille))
    fenetre.blit(vaisseau_taille_2,(1350-500//taille*(taille-1)+500//taille*4+500//(taille*2),600+500//taille))

def titres_et_cadres():
    '''
    Chargement et collage des titres des grilles
    '''
    cadre_texte_gauche = pygame.image.load("ecran.png")
    cadre_texte_gauche.set_colorkey((4,51,61))
    cadre_texte_gauche = pygame.transform.scale(cadre_texte_gauche,(500//taille*6+35,55))
    fenetre.blit(cadre_texte_gauche, (150+500//taille*2-18,10))
    nom_gauche = pygame.image.load("votre_flotte.png").convert_alpha()
    nom_gauche = pygame.transform.scale(nom_gauche,(500//taille*6,60))
    fenetre.blit(nom_gauche,(150+500//taille*2,0))
    cadre_texte_droit = pygame.image.load("ecran.png")
    cadre_texte_droit.set_colorkey((4,51,61))
    cadre_texte_droit = pygame.transform.scale(cadre_texte_droit,(500//taille*6+35,55))
    fenetre.blit(cadre_texte_droit,(1350-500//taille*(taille-1)+500//taille*2-18,10))
    nom_droite = pygame.image.load("flotte adverse.png").convert_alpha()
    nom_droite = pygame.transform.scale(nom_droite,(500//taille*6,73))
    fenetre.blit(nom_droite,(1350-500//taille*(taille-1)+500//taille*2,5))

def ecran_passage_autre_joueur():
    '''
    Affiche l'écran demandant de passer à l'autre joueur
    '''
    fond_global = pygame.image.load("fond_espace.jpg").convert()
    fond_global = pygame.transform.scale(fond_global, (1530,900))
    fenetre.blit(fond_global, (0,0))
    cadre = pygame.image.load("ecran.png").convert()
    cadre.set_colorkey((4,51,61))
    cadre = pygame.transform.scale(cadre,(1000,440))
    fenetre.blit(cadre, (265,150))
    texte = pygame.image.load("passez_au_prochain_joueur.png").convert_alpha()
    texte = pygame.transform.scale(texte, (950,420))
    fenetre.blit(texte,(300,155))

def Menu():
    '''
    Menu de choix du mode de jeu
    '''
    fond_global = pygame.image.load("fond_espace.jpg").convert()
    fond_global = pygame.transform.scale(fond_global, (1530,900))
    fenetre.blit(fond_global, (0,0))
    titre = pygame.image.load("bataille spatiale.png").convert_alpha()
    titre = pygame.transform.scale(titre,(1000,400))
    fenetre.blit(titre, (265,50))
    bouton_1_joueur = pygame.image.load("ecran.png").convert()
    bouton_1_joueur.set_colorkey((4,51,61))
    bouton_1_joueur = pygame.transform.scale(bouton_1_joueur, (700,180))
    fenetre.blit(bouton_1_joueur, (40,530))
    bouton_2_joueur = pygame.image.load("ecran.png").convert()
    bouton_2_joueur.set_colorkey((4,51,61))
    bouton_2_joueur = pygame.transform.scale(bouton_2_joueur, (700,180))
    fenetre.blit(bouton_1_joueur, (1530-40-700,530))
    texte_1_joueur = pygame.image.load("1 joueur.png").convert_alpha()
    texte_1_joueur = pygame.transform.scale(texte_1_joueur, (680,170))
    fenetre.blit(texte_1_joueur, (50,535))
    texte_2_joueur = pygame.image.load("2 joueurs.png").convert_alpha()
    texte_2_joueur = pygame.transform.scale(texte_2_joueur, (680,170))
    fenetre.blit(texte_2_joueur, (1530-40-680,535))

def coordonnee_pixel_grille_gauche(coord):
    return (500//taille*(coord[1])+150, 500//taille*(coord[0])+70)

def coordonnee_pixel_grille_droite(coord):
    return (1350-500//taille*(coord[1])+150, 500//taille*(coord[0])+70)


def affichage_vaisseaux_dans_grille(vaisseau):  #affiche un vaisseaux dans la grille de gauche
    dico_image_vaisseaux={2:vaisseau_taille_2, 3:vaisseau_taille_3, 4:vaisseau_taille_4, 5:vaisseau_taille_5}
    vaisseau_a_afficher=dico_image_vaisseaux[vaisseau[0]]
    print(vaisseau)
    if vaisseau[1][0][1]==vaisseau[1][1][1]:    #on tourne le vaiseau si il est dans le sens vertical
        vaisseau_a_afficher=pygame.transform.rotate(dico_image_vaisseaux[vaisseau[0]], 90)
    fenetre.blit(vaisseau_a_afficher,(coordonnee_pixel_grille_gauche(vaisseau[1][0])[0]+5,coordonnee_pixel_grille_gauche(vaisseau[1][0])[1]+5)) #il y a des petits tweak de position pour avoir des rendu plus propres


def affiche_coups_gauche(grid):
    for i in range (len(grid)):
        for j in range (len(grid)):
            if grid[i][j]==1:
                fenetre.blit(sad_trooper, (coordonnee_pixel_grille_gauche((i,j))[0]+1, coordonnee_pixel_grille_gauche((i,j))[1]+1))
            if grid[i][j]==2:
                fenetre.blit(explosion, (coordonnee_pixel_grille_gauche((i,j))[0]+1, coordonnee_pixel_grille_gauche((i,j))[1]+1))
            
def affiche_coups_droite(grid):
    for i in range (len(grid)):
        for j in range (len(grid)):
            if grid[i][j]==1:
                fenetre.blit(sad_trooper, (coordonnee_pixel_grille_droite((i,j))[0]+1, coordonnee_pixel_grille_droite((i,j))[1]+1))
            if grid[i][j]==2:
                fenetre.blit(explosion, (coordonnee_pixel_grille_droite((i,j))[0]+1, coordonnee_pixel_grille_droite((i,j))[1]+1))

#Fonction d'intéraction avec les clics
def mouseclick(position_souris):
    if menu == 1:
        if nature_action == 0: # on sélectionne les vaisseaux
            if position_souris[0]>=125 and position_souris[0]<=240 and position_souris[1]>= 600 and position_souris[1]<= 640: #vaisseau taille 5
                nature_action = 1
                bateau_selectionné = 5
                bateaux_placés_j1.remove(5)
        if nature_action == 1: # on place ou fait tourner le bâteau
            direction = "b" #b pour bas, d pour droit
            if position_souris[0]>=150 and position_souris[0]<=650 and position_souris[1]>=70 and position_souris[1]<=570:#on est dans la grille
                ligne = (position_souris[1]-70)//taille
                colonne = (position_souris[0]-150)//taille
                creer_vaisseau(taille_vaisseau, case_haut_gauche, direction, grille_placement1)



#Rafraîchissement de l'écran
fond()
grille()
grille_de_gauche()
grille_de_droite()
titres_et_cadres()
texte_principal(0)

#tests
affichage_vaisseaux_dans_grille([5,[(1,2),(1,3)]])
grille_test=[[0 for j in range (10)] for i in range (10)]
grille_test[1][8]=2
grille_test[2][2]=1
grille_test[1][4]=2
affiche_coups_gauche(grille_test)

#ecran_passage_autre_joueur()
#Menu()
pygame.display.flip()
continuer = 1

#Sounds
pygame.mixer.music.load("sounds/Music.mp3")

#Boucle infinie
while continuer:
    for event in pygame.event.get():   #On parcours la liste de tous les événements reçus
        if event.type == QUIT:     #Si un de ces événements est de type QUIT
            continuer = 0   #On arrête la boucle
        elif event.type == MOUSEBUTTONDOWN :
            mouseclick(event.pos)