import random as rd
from back import creer_vaisseau

def nombre_cases_restantes(Grille_placement):
    n = len(Grille_placement)
    nbcasestot = 0
    nbcasesrest = 0
    for i in range(n):
        for j in range(n):
            if Grille_placement[i][j] == 2 :
                nbcasestot += 1
                nbcasesrest +=1
            elif Grille_placement[i][j] == 3:
                nbcasestot +=1
    return nbcasesrest, nbcasestot

def loi_proba_renforts(proba_max, x, nombre_cases_max):
    ''' x est le rapport proportion du joueur/proportion du joueur adverse. On cherche à favoriser le joueur en détresse, donc 
    plus x est petit, plus les chances de voir les renforts sont grandes'''
    y = 3*x
    return proba_max* ((1/y - y)/(1/y + y)+1)/2

def Bernoulli(p):
    x = rd.random()
    if x <= p :
        return True
    return False


def tirage_renforts(Grille_placement1, Grille_placement2):
    ''' Arrivée miraculeuse des renforts '''
    proba_max = .1 # Probabilité maximum d'arrivée des renforts
    nbcasesrest1, nbcasesmax = nombre_cases_restantes(Grille_placement1)
    nbcasesrest2 = nombre_cases_restantes(Grille_placement2)[0]
    proba = loi_proba_renforts(proba_max, nbcasesrest1/nbcasesrest2, nbcasesmax) #Probabilité que les renforts arrivent à ce tour

    arrivée = Bernoulli(proba) # Les renforts vont-ils arriver ?
    return arrivée

def choix_nouveau_vaisseau():
    proba_vaisseau = [0.1, 0.3, 0.35, 0.25] # proba_choix_vaisseau[i] = proba d'obtenir un vaisseau de taille i+2
    x = rd.random()
    somme = 0
    for i in range(4):
        if somme <= x <= somme + proba_vaisseau[i] :
            return i+2
        else :
            somme += proba_vaisseau[i]

def choix_dir() :
    ''' Détermine aléatoirement si le vaisseau est horizontal ou vertical'''
    q = rd.randint(1,2)
    if q == 1 :
        return "b"
    else :
        return "d"

def placement_nouveau_vaisseau(Grille_placement, nombre_vaisseaux):
    ''' Place un nouveau vaisseau dont la taille est déterminée aléatoirement. Celui-ci ne peut être placé sur un vaisseau 
    ni sur une épave. Celui-ci s'affiche sur Grille_placement.
    '''
    taille = choix_nouveau_vaisseau()
    nombre_vaisseaux += 1
    réussi = False
    while not réussi : # Tant que le placement du vaisseau échoue
        x = rd.randint(1,10)
        y = rd.randint(1,10)
        direction = choix_dir()
        vaisseau = creer_vaisseau(taille,  (x,y), direction, Grille_placement) # Choix des cases du nouveau vaisseau

        for case in vaisseau[1] : # Actualisation de Grille_placement
            (x,y) = case
            Grille_placement[x][y] = nombre_vaisseaux
    return Grille_placement, vaisseau

def quart_nouveau_vaisseau(Grille_attaque, nouveau_vaisseau):
    '''Détermine aléatoirement un carré de 5*5 contenant le vaisseau. Réinitialise ce carré sur Grille_attaque, à l'exception des cases
    où un vaisseau a été touché.'''
    n = nouveau_vaisseau[0]
    cases = nouveau_vaisseau[1]
    x,y = cases[0] # Coordonnées de la première case
    if cases[0][0] == cases[-1][0] : # Abscisses égales donc horizontal
        carre_x_min_min = max(0, x - 4) # L'abscisse minimale du carré doit être plus grande
        carre_y_min_min = max(0, y + n - 5) # L'ordonnée minimale du carré doit être plus grande

    else : # Le vaisseau est vertical
        carre_x_min_min = max(0, x + n - 5)
        carre_y_min_min = max(0, y - 4)

    # Choix du carré
    carre_x_min = rd.choice([i for i in range(carre_x_min_min, min(carre_x_min_min+4, x))]) # On choisit aléatoirement de sorte que le vaisseau soit dedans
    carre_y_min = rd.choice([i for i in range(carre_y_min_min, min(carre_y_min_min+4, y))])

    # Vidage de Grille_attaque
    for x in range(carre_x_min, carre_x_min + 5):
        for y in range(carre_y_min, carre_y_min + 5) :
            if Grille_attaque[x][y] == 1 : # Case déjà essayée mais qui était vide
                Grille_attaque[x][y] = 0 # Devient inconnue
    return Grille_attaque

def ev_renforts(Grille_placement1, Grille_attaque2, Grille_placement2,  vaisseaux1):
    ''' Effectue l'événement : "placer, ou non, un vaisseau en renforts pour le joueur 1"
    Prend en argument la grille de placement du joueur 1, et les deux grilles du joueur 2.
    Prend aussi la liste des vaisseaux du joueur 1, pour la modifier si besoin.

    Tire au sort pour savoir si les renforts viennent. (Grille_placement2 influence le tirage au sort).
    S'il n'y en a pas, rien ne se passe.
    Sinon, on tire au sort la taille et la position du nouveau vaisseau.

    La grille de placement du joueur 1 est modifiée en conséquence.
    On vide un carré de 5*5 aléatoire autour du nouveau vaisseau dans Grille_attaque2. '''

    arrivée = tirage_renforts(Grille_placement1, Grille_placement2)
    if arrivée :
        print("Des renforts sont arrivés !")
        nombre_vaisseaux1 = len(vaisseaux1)
        Grille_placement1, nouveau_vaisseau = placement_nouveau_vaisseau(Grille_placement1, nombre_vaisseaux1)
        vaisseaux1.append(nouveau_vaisseau)
        Grille_attaque2 = quart_nouveau_vaisseau(Grille_attaque2, nouveau_vaisseau)
    return Grille_placement1, Grille_attaque2, vaisseaux1

'''   
grille = [[0 for i in range(10)] for j in range(10)]
grille[0][0] = 1
grille[0][1] = 2
grille[1][2] = 1
nouveau_vaisseau = [3,[(1,2), (1,3), (1,4)],[]]
print(quart_nouveau_vaisseau(grille, nouveau_vaisseau))
'''
        
def events(Grille_placement1, Grille_attaque2, Grille_placement2,  vaisseaux1, Events = {"renforts": True}):
    '''Effectue tous les événements autorisés par le joueur.
    Events est un dictionnaire : {"Event1" : True, "Events2" : False, ...}'''
    if Events["renforts"] :
        grille_placement1, grille_attaque2, vaisseaux1 = ev_renforts(Grille_placement1, Grille_attaque2, Grille_placement2,  vaisseaux1)
    return grille_placement1, grille_attaque2, vaisseaux1

