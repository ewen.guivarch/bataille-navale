import pygame
from pygame.locals import *
from back import *
from ia_init_placement import *
from choix_tir_ia import *
from moviepy.editor import *

pygame.init()

#Ouverture de la fenêtre Pygame
#initialisation des images récurrentes
fenetre = pygame.display.set_mode((1530, 900),RESIZABLE)
taille = 10
vaisseau_taille_5 = pygame.image.load("super destroyer.png").convert_alpha()
vaisseau_taille_5 = pygame.transform.scale(vaisseau_taille_5,(500//taille*5-10,500//taille-10))
vaisseau_taille_4 = pygame.image.load("star_destroyer.png").convert()
vaisseau_taille_4.set_colorkey((0,0,0))
vaisseau_taille_4 = pygame.transform.scale(vaisseau_taille_4,(500//taille*4-10,500//taille-10))
vaisseau_taille_3 = pygame.image.load("corvette_cr90.png").convert_alpha()
vaisseau_taille_3 = pygame.transform.scale(vaisseau_taille_3,(500//taille*3-10,500//taille-10))
vaisseau_taille_3_bis = pygame.image.load("corvette_cr90.png").convert_alpha()
vaisseau_taille_3_bis = pygame.transform.scale(vaisseau_taille_3_bis,(500//taille*3-10,500//taille-10))
vaisseau_taille_2 = pygame.image.load("x_wing.png").convert_alpha()
vaisseau_taille_2 = pygame.transform.scale(vaisseau_taille_2,(500//taille*2-10,500//taille-10))
croix = pygame.image.load("croix bleu.png").convert_alpha()
sad_trooper=pygame.image.load("sad_trooper.png").convert()
sad_trooper=pygame.transform.scale(sad_trooper, (500//taille-1, 500//taille-1))
explosion=pygame.image.load("explosion.png").convert_alpha()
explosion=pygame.transform.scale(explosion, (500//taille-1, 500// taille-1))

# Importation des sons
tirLaser1 = pygame.mixer.Sound("Sounds/tirLaser1.wav")
tirLaser2 = pygame.mixer.Sound("Sounds/tirLaser2.wav")
explosionSound = pygame.mixer.Sound("Sounds/vaisseauBoum.wav")
bipbipbip = pygame.mixer.Sound("Sounds/bipbipbip.wav")
hyperespace = pygame.mixer.Sound("Sounds/Hyperespace.wav")

menu = 0 
#vaut 0 pour le menu initial, 1 pour le 1er joueur, 2 pour le 2nd, 3 pour celui de transition vers 2, 4 transition vers 1, 5 game over
#les menu en .5 permettent au joueur de faire un dernier clic où il voit les actions qu'il a fait avant de passer l'écran au prochain joueur
#pour le mode joueur vs ia: 11 pour le joueur, 12 pour l'ia, 5 game over

etat = 0  #l'état définit la manière d'attaquer de l'ia, cf choix_tir_ia
cases_restantes_ia = [ (i,j) for i in range(10) for j in range(10) ]  #liste mémorisant où l'ia n'a pas tiré
liste_touchee_ia = [] #liste mémorisant les cases touchées par l'ia

nature_action = 0 
#0 pour la sélection d'un bâteau en placement, 1 pour un clic sur la grille pour le placement,
# 2 pour un clic sur la grille pour une attaque, 10 pour le placement par l'ia
gagnant = -1 
#0:ia, 1:j1, 2:j2
vaisseaux_à_placer_j1 = [2,3,'3b',4,5]
vaisseaux_à_placer_j2 = [2,3,'3b',4,5]
vaisseau_selectionné = -1
dico_vaisseaux1={}
dico_vaisseaux_détruits_1={}
dico_vaisseaux2={}
dico_vaisseaux_détruits_2={}
dico_vaisseaux_placés_en_phase_de_placement1 = {}
dico_vaisseaux_placés_en_phase_de_placement2 = {}
grille_position1=[[" " for j in range(taille)]for i in range(taille)]
grille_position2=[[" " for j in range(taille)]for i in range(taille)]
grille_attaque1=[[" " for j in range(taille)]for i in range(taille)]
grille_attaque2=[[" " for j in range(taille)]for i in range(taille)]

direction = "d" #b pour bas, d pour droit

def restart():
    '''
    Permet de réinitialiser les variables globales du jeu afin de le préparer à une nouvelle partie.
    Cette fonction est appelée à la fin d'une partie.
    '''
    global menu, nature_action, gagnant, vaisseaux_à_placer_j1, vaisseaux_à_placer_j2, vaisseau_selectionné, dico_vaisseaux1, dico_vaisseaux2, grille_attaque1, grille_attaque2, grille_position1, grille_position2, direction, dico_vaisseaux_détruits_1, dico_vaisseaux_détruits_2, dico_vaisseaux_placés_en_phase_de_placement1, dico_vaisseaux_placés_en_phase_de_placement2
    menu = 0 #vaut 0 pour le menu initial, 1 pour le 1er joueur, 2 pour le 2nd, 3 pour celui de transition vers 2, 4 transition vers 1, 5 game over
    #les menu en .5 permettent au joueur de faire un dernier clic où il voit les actions qu'il a fait avant de passer l'écran au prochain joueur

    nature_action = 0 #0 pour la sélection d'un bâteau en placement, 1 pour un clic sur la grille pour le placement,
    # 2 pour un clic sur la grille pour une attaque
    gagnant = -1 #0:ia, 1:j1, 2:j2
    vaisseaux_à_placer_j1 = [2,3,'3b',4,5]
    vaisseaux_à_placer_j2 = [2,3,'3b',4,5]
    vaisseau_selectionné = -1
    dico_vaisseaux1={}
    dico_vaisseaux2={}
    dico_vaisseaux_détruits_2={}
    dico_vaisseaux_détruits_1={}
    dico_vaisseaux_placés_en_phase_de_placement1 = {}
    dico_vaisseaux_placés_en_phase_de_placement2 = {}
    grille_position1=[[" " for j in range(taille)]for i in range(taille)]
    grille_position2=[[" " for j in range(taille)]for i in range(taille)]
    grille_attaque1=[[" " for j in range(taille)]for i in range(taille)]
    grille_attaque2=[[" " for j in range(taille)]for i in range(taille)]

    direction = "d" #b pour bas, d pour droit
#    print("je restart")
    Menu()
    pygame.display.flip()

def fond():
    '''
    Chargement et collage du fond.
    Affiche aussi le nom du joueur dont l'écran est affiché.
    '''
    fond_global = pygame.image.load("fond cockpit.jpg").convert()
    fond_global = pygame.transform.scale(fond_global, (1530,900))
    fenetre.blit(fond_global, (0,0))
    fond_grille = pygame.image.load("fond_espace.jpg").convert()
    fond_grille = pygame.transform.scale(fond_grille, (500//taille*(taille), 500//taille*(taille)))
    logo = pygame.image.load("Logo_La_Guérande.png").convert_alpha()
    logo = pygame.transform.scale(logo, (150,150))
    fenetre.blit(fond_grille, (150,70))
    fenetre.blit(fond_grille, (1350-500//taille*(taille-1),70))
    fenetre.blit(logo, (0,0))
    if menu == 1 or menu == 11 or menu == 3 or menu == 3.5 or menu == 0 or menu == 12:
        cadre = pygame.image.load("ecran.png").convert()
        cadre = pygame.transform.scale(cadre,(100,100))
        fenetre.blit(cadre, (722, 20))
        texte = pygame.image.load("J1.png")
        texte = pygame.transform.scale(texte, (100,100))
        fenetre.blit(texte, (725,20))
    if menu == 2 or menu == 4 or menu == 4.5 :
        cadre = pygame.image.load("ecran.png").convert()
        cadre = pygame.transform.scale(cadre,(100,100))
        fenetre.blit(cadre, (722, 20))
        texte = pygame.image.load("J2.png")
        texte = pygame.transform.scale(texte, (100,100))
        fenetre.blit(texte, (725,20))


def grille():
    '''
    Chargement et collage des grilles
    '''
    carre = pygame.image.load("oeuvre_d_art.png").convert()
    carre.set_colorkey((0,0,0))
    carre = pygame.transform.scale(carre, (500//taille,500//taille))
    for i in range (taille):
        for j in range (taille):
            fenetre.blit(carre, (500//taille*i+150,500//taille*j+70))
            fenetre.blit(carre, (1350-500//taille*i,500//taille*j+70))



def texte_principal(message):
    '''
    affichage des textes en bas de l'écran
    '''
    dico_text={0:"selectionnez.png",1:"positionnez le.png",2:"cliquez pour tirer.png",3:"vaisseau détruit.png",4:"raté.png", 5:"touché.png",6:"vaisseau horizontale.png",7:"vaisseau verticale.png"}
    cadre_info = pygame.image.load("ecran.png").convert()
    cadre_info = pygame.transform.scale(cadre_info,(1300, 80))
    fenetre.blit(cadre_info, (120, 710) )
    texte= pygame.image.load(dico_text[message]).convert_alpha()
    texte = pygame.transform.scale(texte, (1200, 70))
    fenetre.blit(texte, (170,715))

def grille_de_gauche():
    '''
    Chargement et collage des icônes des vaisseaux côté gauche
    '''
    cadre_5 = pygame.image.load("ecran.png").convert()
    cadre_5.set_colorkey((4,51,61))
    cadre_5 = pygame.transform.scale(cadre_5,(500+35,500//taille*2+10))
    fenetre.blit(cadre_5,(150-25,600-10))
    fenetre.blit(vaisseau_taille_5,(150,600))
    fenetre.blit(vaisseau_taille_4,(150,600+ 500//taille))
    fenetre.blit(vaisseau_taille_3,(150+500-500//taille*3,600))
    fenetre.blit(vaisseau_taille_3_bis,(150+500-500//taille*3,600+500//taille))
    fenetre.blit(vaisseau_taille_2,(150+500//taille*4+500//(taille*2),600+500//taille))

def grille_de_droite():
    '''
    Chargement et collage des icônes des vaisseaux côté droit
    '''
    cadre_5 = pygame.image.load("ecran.png").convert()
    cadre_5.set_colorkey((4,51,61))
    cadre_5 = pygame.transform.scale(cadre_5,(500+35,500//taille*2+10))
    fenetre.blit(cadre_5,(1350-500//taille*(taille-1)-25,600-10))
    fenetre.blit(vaisseau_taille_5,(1350-500//taille*(taille-1),600))
    fenetre.blit(vaisseau_taille_4,(1350-500//taille*(taille-1),600+ 500//taille))
    fenetre.blit(vaisseau_taille_3,(1350-500//taille*(taille-1)+500-500//taille*3,600))
    fenetre.blit(vaisseau_taille_3_bis,(1350-500//taille*(taille-1)+500-500//taille*3,600+500//taille))
    fenetre.blit(vaisseau_taille_2,(1350-500//taille*(taille-1)+500//taille*4+500//(taille*2),600+500//taille))

def titres_et_cadres():
    '''
    Chargement et collage des titres des grilles.
    Charge aussi le bouton de rotation des vaisseaux.
    '''
    cadre_texte_gauche = pygame.image.load("ecran.png")
    cadre_texte_gauche.set_colorkey((4,51,61))
    cadre_texte_gauche = pygame.transform.scale(cadre_texte_gauche,(500//taille*6+35,55))
    fenetre.blit(cadre_texte_gauche, (150+500//taille*2-18,10))
    nom_gauche = pygame.image.load("votre_flotte.png").convert_alpha()
    nom_gauche = pygame.transform.scale(nom_gauche,(500//taille*6,60))
    fenetre.blit(nom_gauche,(150+500//taille*2,7))
    cadre_texte_droit = pygame.image.load("ecran.png")
    cadre_texte_droit.set_colorkey((4,51,61))
    cadre_texte_droit = pygame.transform.scale(cadre_texte_droit,(500//taille*6+35,55))
    fenetre.blit(cadre_texte_droit,(1350-500//taille*(taille-1)+500//taille*2-18,10))
    nom_droite = pygame.image.load("flotte adverse.png").convert_alpha()
    nom_droite = pygame.transform.scale(nom_droite,(500//taille*6,73))
    fenetre.blit(nom_droite,(1350-500//taille*(taille-1)+500//taille*2,0))
    bouton_rotation = pygame.image.load("rotation.png") #660, 875 590, 700
    bouton_rotation = pygame.transform.scale(bouton_rotation, (150,150))
    fenetre.blit(bouton_rotation, (690, 580))

def ecran_passage_autre_joueur():
    '''
    Affiche l'écran demandant de passer à l'autre joueur.
    Un clic de plus permet de quitter cet écran pour que l'autre joueur puisse faire son tour.
    '''
    fond_global = pygame.image.load("fond_espace.jpg").convert()
    fond_global = pygame.transform.scale(fond_global, (1530,900))
    fenetre.blit(fond_global, (0,0))
    cadre = pygame.image.load("ecran.png").convert()
    cadre.set_colorkey((4,51,61))
    cadre = pygame.transform.scale(cadre,(1000,440))
    fenetre.blit(cadre, (265,150))
    texte = pygame.image.load("passez_au_prochain_joueur.png").convert_alpha()
    texte = pygame.transform.scale(texte, (950,420))
    fenetre.blit(texte,(300,155))

def ecran_ia():
    '''
    Affiche l'écran disant que l'ia travaille
    '''
    fond_global = pygame.image.load("fond_espace.jpg").convert()
    fond_global = pygame.transform.scale(fond_global, (1530,900))
    fenetre.blit(fond_global, (0,0))
    cadre = pygame.image.load("ecran.png").convert()
    cadre.set_colorkey((4,51,61))
    cadre = pygame.transform.scale(cadre,(1000,440))
    fenetre.blit(cadre, (265,150))
    texte = pygame.image.load("tour ia.png").convert_alpha()
    texte = pygame.transform.scale(texte, (950,420))
    fenetre.blit(texte,(300,155))



def ecran_de_fin():
    '''
    Affiche l'écran de fin de partie, avec un message indiquant le joueur gagnant.
    '''
    global gagnant,menu
    menu = 5
    fond_global = pygame.image.load("fond_espace.jpg").convert()
    fond_global = pygame.transform.scale(fond_global, (1530,900))
    fenetre.blit(fond_global, (0,0))
    cadre = pygame.image.load("ecran.png").convert()
    cadre.set_colorkey((4,51,61))
    cadre = pygame.transform.scale(cadre,(1000,440))
    fenetre.blit(cadre, (265,150))
    if gagnant == 0:
        texte = pygame.image.load("GO ia.png")
        texte = pygame.transform.scale(texte, (950,420))
        fenetre.blit(texte, (300,155)) 
    if gagnant == 1:
        texte = pygame.image.load("GO j1.png")
        texte = pygame.transform.scale(texte, (950,420))
        fenetre.blit(texte, (300,155))    
    if gagnant == 2:
        texte = pygame.image.load("GO j2.png")
        texte = pygame.transform.scale(texte, (950,420))
        fenetre.blit(texte, (300,155))   



def Menu():
    '''
    Menu de choix du mode de jeu
    Affiche tout le menu et les boutons de choix "1 joueur" et "2 joueurs"
    '''
    fond_global = pygame.image.load("fond_espace.jpg").convert()
    fond_global = pygame.transform.scale(fond_global, (1530,900))
    fenetre.blit(fond_global, (0,0))
    titre = pygame.image.load("bataille spatiale.png").convert_alpha()
    titre = pygame.transform.scale(titre,(1000,400))
    fenetre.blit(titre, (265,50))
    bouton_1_joueur = pygame.image.load("ecran.png").convert()
    bouton_1_joueur.set_colorkey((4,51,61))
    bouton_1_joueur = pygame.transform.scale(bouton_1_joueur, (700,180))
    fenetre.blit(bouton_1_joueur, (40,530))
    bouton_2_joueur = pygame.image.load("ecran.png").convert()
    bouton_2_joueur.set_colorkey((4,51,61))
    bouton_2_joueur = pygame.transform.scale(bouton_2_joueur, (700,180))
    fenetre.blit(bouton_1_joueur, (1530-40-700,530))
    texte_1_joueur = pygame.image.load("1 joueur.png").convert_alpha()
    texte_1_joueur = pygame.transform.scale(texte_1_joueur, (680,170))
    fenetre.blit(texte_1_joueur, (50,535))
    texte_2_joueur = pygame.image.load("2 joueurs.png").convert_alpha()
    texte_2_joueur = pygame.transform.scale(texte_2_joueur, (680,170))
    fenetre.blit(texte_2_joueur, (1530-40-680,535))

def coordonnee_pixel_grille_gauche(coord):
    '''
    Donne les coordonnées des pixels correspondant à une case "coord" dans la grille de droite
    '''
    return (500//taille*(coord[1])+150, 500//taille*(coord[0])+70)

def coordonnee_pixel_grille_droite(coord):
    '''
    Donne les coordonnées des pixels correspondant à une case "coord" dans la grille de droite
    '''
    return (1350-500//taille*(taille-1)+500//taille*(coord[1]), 500//taille*(coord[0])+70)


def affichage_vaisseaux_dans_grille(vaisseau):  #affiche un vaisseaux dans la grille de gauche
    '''
    Affiche le vaisseau "vaisseau" dans la grille de gauche
    '''
    dico_image_vaisseaux={2:vaisseau_taille_2, 3:vaisseau_taille_3, 4:vaisseau_taille_4, 5:vaisseau_taille_5}
    vaisseau_a_afficher=dico_image_vaisseaux[vaisseau[0]]
#    print(vaisseau)
    if vaisseau[1][0][1]==vaisseau[1][1][1]:    #on tourne le vaiseau si il est dans le sens vertical
        vaisseau_a_afficher=pygame.transform.rotate(dico_image_vaisseaux[vaisseau[0]], 90)
    fenetre.blit(vaisseau_a_afficher,(coordonnee_pixel_grille_gauche(vaisseau[1][0])[0]+5,coordonnee_pixel_grille_gauche(vaisseau[1][0])[1]+5)) #il y a des petits tweak de position pour avoir des rendu plus propres

def affichage_croix_gauche(dico): #affiche une croix sur un vaisseau détruit à gauche
    '''
    Affiche des croix sur les vaisseaux du dico en dessous de la grille gauche.
    Cela est utile pour la phase de placement pour dire que ce vaisseau est déjà placé.
    Cela sert aussi lors de la phase d'attaque pour dire que ce vaisseau est détruit
    '''
    global croix
    for vaisseau in dico:
        if vaisseau == '3b':
            croix = pygame.transform.scale(croix, (500//taille*3,500//taille))
        else:
            croix = pygame.transform.scale(croix, (500//taille*vaisseau,500//taille))
        if vaisseau == 3:
            fenetre.blit(croix, (650-500//taille*3, 600))
        elif vaisseau == '3b':
                fenetre.blit(croix, (650-500//taille*3,600+500//taille))
        elif vaisseau == 2:
            fenetre.blit(croix, (150+500//taille*4+500//(taille*2), 600+500//taille))
        elif vaisseau == 4:
            fenetre.blit(croix, (150, 600+500//taille))
        elif vaisseau == 5:
            fenetre.blit(croix, (150, 600))

def affichage_croix_droite(dico): #affiche une croix sur un vaisseau détruit à droite
    '''
    Affiche des croix sur les vaisseaux du dico en dessous de la grille droite.
    Cela est utile pour la phase de placement pour dire que ce vaisseau est déjà placé.
    Cela sert aussi lors de la phase d'attaque pour dire que ce vaisseau est détruit
    '''
    global croix
    for vaisseau in dico:
        if vaisseau == '3b':
            croix = pygame.transform.scale(croix, (500//taille*3,500//taille))
        else:
            croix = pygame.transform.scale(croix, (500//taille*vaisseau,500//taille))
        if vaisseau == 3:
            fenetre.blit(croix, (1350-500//taille*(taille-1)+500-500//taille*3, 600))
        elif vaisseau == '3b':
            fenetre.blit(croix, (1350-500//taille*(taille-1)+500-500//taille*3,600+500//taille))
        elif vaisseau == 2:
            fenetre.blit(croix, (1350-500//taille*(taille-1)+500//taille*4+500//(taille*2), 600+500//taille))
        elif vaisseau == 4:
            fenetre.blit(croix, (1350-500//taille*(taille-1), 600+500//taille))
        elif vaisseau == 5:
            fenetre.blit(croix, (1350-500//taille*(taille-1), 600))

def affiche_coups_gauche(grid):
    '''
    Affiche les coups qui ont été effectués sur la grille de gauche de l'écran
    '''
    for i in range (len(grid)):
        for j in range (len(grid)):
            if grid[i][j]==1:
                fenetre.blit(sad_trooper, (coordonnee_pixel_grille_gauche((i,j))[0]+1, coordonnee_pixel_grille_gauche((i,j))[1]+1))
            if grid[i][j]==2:
                fenetre.blit(explosion, (coordonnee_pixel_grille_gauche((i,j))[0]+1, coordonnee_pixel_grille_gauche((i,j))[1]+1))
            
def affiche_coups_droite(grid):
    '''
    Affiche les coups qui ont été effectués sur la grille de droite de l'écran
    '''
    for i in range (len(grid)):
        for j in range (len(grid)):
            if grid[i][j]==1:
                fenetre.blit(sad_trooper, (coordonnee_pixel_grille_droite((i,j))[0]+1, coordonnee_pixel_grille_droite((i,j))[1]+1))
            if grid[i][j]==2:
                fenetre.blit(explosion, (coordonnee_pixel_grille_droite((i,j))[0]+1, coordonnee_pixel_grille_droite((i,j))[1]+1))


def creer_vaisseau_dans_grille(n,case,dir,grille,dico_vaisseaux_placés_en_phase_de_placement) :
    ''' 
    Créé un vaisseau de n cases, de case de départ case (tuple), et de direction dir ("b" pour bas, "d" pour droite).
    Renvoie False si on ne peut pas mettre le vaisseau
    '''
    x,y = case
    n_bis = n
    if n == '3b':
        n=3
    if dir == "b" and x+n <= 10 : # Ne sortons pas de la grille
        cases = [] #Liste des cases occupées par le vaisseau
        for i in range(n) :
            if grille[x+i][y] == " " : #Si aucun vaisseau n'occupe déjà cette case
                cases.append((x+i,y))
            else : return(False)
    elif dir == "d" and y+n <= 10 : # Ne sortons pas de la grille
        cases = [] #Liste des cases occupées par le vaisseau
        for i in range(n) :
            if grille[x][y+i] == " " : #Si aucun vaisseau n'occupe déjà cette case
                cases.append((x,y+i))
            else :
#                print('false d') 
                return(False)
    else : # Le placement est incorrect.
#        print('false dir', dir, x, y, n)
        return False
    for case_de_vaisseau in cases:
        grille[case_de_vaisseau[0]][case_de_vaisseau[1]]=str(n_bis)
    dico_vaisseaux_placés_en_phase_de_placement[n_bis]=n_bis
    return([n,cases,[],n_bis])

def attaque_de_la_case(case,grille_placement,vaisseaux, grille_attaque) :
    '''
    Le joueur attaquant, avec sa grille d'attaque grille_attaque attaque l'autre joueur en (x,y).
    L'attaqué voit les tirs sur grille_placement. Le placement de l'autre joueur est donné par vaisseaux et nombrevaisseaux
    '''
    global gagnant
    tirLaser1.play()
    global dico_vaisseaux_détruits_1, dico_vaisseaux_détruits_2, menu
    (x,y) = case
#    print("grille_attaque",grille_attaque)
#    print("grille_placement", grille_placement)
    if grille_placement[x][y] == " " and grille_attaque[x][y]==" " : # Dans l'eau
        grille_attaque[x][y] = 1
#        print("tir dans l'eau")
        texte_principal(4)
        return("raté") # Notifions qu'un tir a été effectué
    elif grille_attaque[x][y] == " " and grille_placement[x][y]!=" ": # Un vaisseau était là, intact, et on n'avait pas encore tiré ici.
#        print("tir sur un vaisseau", grille_placement, vaisseaux)
        explosionSound.play()
        if grille_placement[x][y] == '3b':
            nBat = '3b'
        else:
            nBat = int(grille_placement[x][y]) # On récupère l'indice du vaisseau
        vaisseau = vaisseaux[nBat] # On récupère le vaisseau correspondant
        grille_attaque[x][y] = 2 # On actualise la grille des attaques du joueur attaquant
        vaisseau[2].append((x,y)) # On place la case dans la liste des cases touchées du vaisseau
        texte_principal(5)
        texte = "touché"
        if len(vaisseau[2])==len(vaisseau[1]): # Le vaisseau est détruit
            explosionSound.play()
            if menu == 2 or menu == 12:
                dico_vaisseaux_détruits_1[vaisseau[3]] = vaisseau[3]
            elif menu == 1 or menu == 11:
                dico_vaisseaux_détruits_2[vaisseau[3]] = vaisseau[3]
#            print("Le vaisseau ", vaisseau[3]," a été détruit")
            texte_principal(3)
            texte = "vaisseau détruit"
        if len(dico_vaisseaux_détruits_1) == 5:
            if menu < 10:
                gagnant= 2
            else :
                gagnant = 0
        elif len(dico_vaisseaux_détruits_2) == 5:
            gagnant = 1
        return(texte) #Touché
    else:
        return(False) #la case était déjà attaquée



#Rafraîchissement de l'écran
def Rafraîchissement_ecran():
    '''
    Cette fonction permet d'afficher les différentes parties graphique du menu habituel du jeu.
    Elle affiche le fond, les grilles, les vaisseaux sous les grilles et les croix sur ces éventuels vaisseaux
    '''
    fond()
    grille()
    grille_de_gauche()
    grille_de_droite()
    titres_et_cadres()
    if menu == 1 or menu == 3.5 or menu == 11 or menu == 12:
        affichage_croix_gauche(dico_vaisseaux_détruits_1)
        affichage_croix_droite(dico_vaisseaux_détruits_2)
    elif menu == 2 or menu == 4.5:
        affichage_croix_gauche(dico_vaisseaux_détruits_2)
        affichage_croix_droite(dico_vaisseaux_détruits_1)
    #menu vaut 0 (on commence la partie), 3.5, 4.5, 1, 2


#Fonction d'intéraction avec les clics
def mouseclick(position_souris):
    '''
    Cette fonction gère tous les clics de souris.
    Elle fonctionne avec différents états pour savoir dans quelle phase de jeu on se trouve, et donc quels clics accepter et que faire de ces clics.
    Le menu 0 gère le menu initial du jeu permettant le choix des joueurs.
    Les menus < 10 gèrent le mode 2 joueurs.
    Les menus >=10 gèrent le mode 1 joueur.
    Après cela, c'est la variable nature_action qui permet de savoir que faire.
    En vallant 0 ou 1, elle permet de gérer la phase de placement des vaisseaux.
    En vallant 2, elle permet de gérer la phase d'attaque.
    '''
    global nature_action
    global vaisseau_selectionné
    global direction
    global menu
    global etat
    global cases_restantes_ia
    global gagnant
    global liste_touchee_ia
    global dico_vaisseaux2
    global grille_position2
    # trait = pygame.image.load("bleu.png")
    # trait = pygame.transform.scale(test, (500//taille*2, 10))
    # fenetre.blit(test, (140+500//taille*4+500//(taille*2),600+500//taille))
#    print("je cherche dans quel menu je suis")
    if menu == 0:
    #menu initial du jeu, utile pour le choix des joueurs
        if position_souris[0]>=790 and position_souris[0]<=1490 and position_souris[1]>=530 and position_souris[1]<=530+180:
            Rafraîchissement_ecran()
            menu = 1
#            print('kgrf')
            texte_principal(0)
        if position_souris[0]>=40 and position_souris[0]<=740 and position_souris[1]>=530 and position_souris[1]<=530+180:
            menu = 11
#            carre_bleu_test = pygame.image.load("bleu.png").convert_alpha()
#            carre_bleu_test = pygame.transform.scale(carre_bleu_test, (700,180))
#            print("je vais dans le menu 11")
#            print('kgrf')
            Rafraîchissement_ecran()
            texte_principal(0)

    elif menu == 1:
    #menu du premier joueur
        if nature_action == 0: # on sélectionne les vaisseaux
            #vaisseau 5
            if position_souris[0]>=140 and position_souris[0]<=125+500//taille*5 and position_souris[1]>= 600 and position_souris[1]<= 640:
                if 5 in vaisseaux_à_placer_j1:
                    nature_action = 1
                    vaisseau_selectionné = 5
#                   print("Bâteau 5 sélectionné")
                    texte_principal(1) # on change le message pour "placez le vaisseau"
                    affichage_croix_gauche(dico_vaisseaux_placés_en_phase_de_placement1)
            #vaisseau 4
            if position_souris[0]>=140 and position_souris[0]<=350 and position_souris[1]>= 600+500//taille and position_souris[1]<= 640+500//taille:
                if 4 in vaisseaux_à_placer_j1:    
                    nature_action = 1
                    vaisseau_selectionné = 4
#                    print("Bâteau 4 sélectionné")
                    texte_principal(1) # on change le message pour "placez le vaisseau"
                    affichage_croix_gauche(dico_vaisseaux_placés_en_phase_de_placement1)
            #vaisseau 3 du haut
            if position_souris[0]>=140+500-500//taille*3 and position_souris[0]<=140+500 and position_souris[1]>= 600 and position_souris[1]<= 640:
                if 3 in vaisseaux_à_placer_j1:
                    nature_action = 1
                    vaisseau_selectionné = 3
#                    print("Bâteau 3 sélectionné")
                    texte_principal(1) # on change le message pour "placez le vaisseau"
                    affichage_croix_gauche(dico_vaisseaux_placés_en_phase_de_placement1)
            #vaisseau 3 du bas
            if position_souris[0]>=140+500-500//taille*3 and position_souris[0]<=140+500 and position_souris[1]>= 600+500//taille and position_souris[1]<= 640+500//taille:
                if '3b' in vaisseaux_à_placer_j1:
                    nature_action = 1
                    vaisseau_selectionné = '3b'
#                    print("Bâteau 3b sélectionné")
                    texte_principal(1) # on change le message pour "placez le vaisseau"
                    affichage_croix_gauche(dico_vaisseaux_placés_en_phase_de_placement1)
            #vaisseau 2
            if position_souris[0]>=140+500//taille*4+500//(taille*2) and position_souris[0]<=140+500//taille*4+500//(taille*2)+500//taille*2 and position_souris[1]>= 600+500//taille and position_souris[1]<= 640+500//taille:
                if 2 in vaisseaux_à_placer_j1:
                    nature_action = 1
                    vaisseau_selectionné = 2
#                    print("Bâteau 2 sélectionné")
                    texte_principal(1) # on change le message pour "placez le vaisseau"
                    affichage_croix_gauche(dico_vaisseaux_placés_en_phase_de_placement1)
        if nature_action == 1: # on place ou fait tourner le bâteau
            if position_souris[0]>=150 and position_souris[0]<=650 and position_souris[1]>=70 and position_souris[1]<=570:#on est dans la grille
                ligne = (position_souris[1]-70)//50
                colonne = (position_souris[0]-150)//50
                if vaisseau_selectionné in vaisseaux_à_placer_j1:
                    v=creer_vaisseau_dans_grille(vaisseau_selectionné, (ligne, colonne), direction, grille_position1,dico_vaisseaux_placés_en_phase_de_placement1)
                    if v:
                        dico_vaisseaux1[vaisseau_selectionné]=v
                        direction = "d"
                        nature_action = 0
                        vaisseaux_à_placer_j1.remove(vaisseau_selectionné)
#                        print (dico_vaisseaux1)
                        texte_principal(0)
                        affichage_croix_gauche(dico_vaisseaux_placés_en_phase_de_placement1)
            if position_souris[0]>=690 and position_souris[0]<=690+150 and  position_souris[1]>=580 and position_souris[1]<=580+150:
#                print('rotation', direction)
                if direction == "b":
                    direction ="d"
                    texte_principal(6)
                else:
                    direction = "b"
                    texte_principal(7)
            for image_vaisseaux in dico_vaisseaux1:
                affichage_vaisseaux_dans_grille(dico_vaisseaux1[image_vaisseaux])
        if nature_action == 2: #phase d'attaque!
            if position_souris[0]>=1350-500//taille*(taille-1) and position_souris[0]<=1350-500//taille*(taille-1)+500//taille*taille and position_souris[1]>=70 and position_souris[1]<=570:#on est dans la grille de droite
                ligne = (position_souris[1]-70)//50
#                print("je suis dans la grille de droite")
                colonne = (position_souris[0]-(1350-500//taille*(taille-1)))//50
                v=attaque_de_la_case((ligne,colonne),grille_position2, dico_vaisseaux2,grille_attaque1)
#                print("v, ",v)
                if v == "vaisseau détruit":
                    menu = 3.5
                    Rafraîchissement_ecran()
                    for image_vaisseaux in dico_vaisseaux1:
                        affichage_vaisseaux_dans_grille(dico_vaisseaux1[image_vaisseaux])
                    affiche_coups_droite(grille_attaque1)
                    affiche_coups_gauche(grille_attaque2)
                    texte_principal(3)
                elif v == "raté":
                    menu = 3.5
                    Rafraîchissement_ecran()
                    for image_vaisseaux in dico_vaisseaux1:
                        affichage_vaisseaux_dans_grille(dico_vaisseaux1[image_vaisseaux])
                    affiche_coups_droite(grille_attaque1)
                    affiche_coups_gauche(grille_attaque2)
                    texte_principal(4)
                elif v == "touché":
                    menu = 3.5      
                    Rafraîchissement_ecran()
                    for image_vaisseaux in dico_vaisseaux1:
                        affichage_vaisseaux_dans_grille(dico_vaisseaux1[image_vaisseaux])
                    affiche_coups_droite(grille_attaque1)
                    affiche_coups_gauche(grille_attaque2)
                    texte_principal(5)
                elif v:
                    menu = 3.5
                    Rafraîchissement_ecran()
                    for image_vaisseaux in dico_vaisseaux1:
                        affichage_vaisseaux_dans_grille(dico_vaisseaux1[image_vaisseaux])
                    affiche_coups_droite(grille_attaque1)
                    affiche_coups_gauche(grille_attaque2)

        if nature_action == 0 and vaisseaux_à_placer_j1==[]:
            menu = 3.5
            Rafraîchissement_ecran()
            for image_vaisseaux in dico_vaisseaux1:
                affichage_vaisseaux_dans_grille(dico_vaisseaux1[image_vaisseaux])
            affiche_coups_droite(grille_attaque1)
            affiche_coups_gauche(grille_attaque2)

    elif menu == 2:
    #menu du 2nd joueurs
        if nature_action == 0: # on sélectionne les vaisseaux
            pygame.display.flip()
            #vaisseau 5
            if position_souris[0]>=140 and position_souris[0]<=125+500//taille*5 and position_souris[1]>= 600 and position_souris[1]<= 640:
                if 5 in vaisseaux_à_placer_j2:
                    nature_action = 1
                    vaisseau_selectionné = 5
#                    print("Bâteau 5 sélectionné")
                    texte_principal(1) # on change le message pour "placez le vaisseau"
                    affichage_croix_gauche(dico_vaisseaux_placés_en_phase_de_placement2)
            #vaisseau 4
            if position_souris[0]>=140 and position_souris[0]<=350 and position_souris[1]>= 600+500//taille and position_souris[1]<= 640+500//taille:
                if 4 in vaisseaux_à_placer_j2:
                    nature_action = 1
                    vaisseau_selectionné = 4
 #                   print("Bâteau 4 sélectionné")
                    texte_principal(1) # on change le message pour "placez le vaisseau"
                    affichage_croix_gauche(dico_vaisseaux_placés_en_phase_de_placement2)
            #vaisseau 3 du haut
            if position_souris[0]>=140+500-500//taille*3 and position_souris[0]<=140+500 and position_souris[1]>= 600 and position_souris[1]<= 640:
                if 3 in vaisseaux_à_placer_j2:
                    nature_action = 1
                    vaisseau_selectionné = 3
#                    print("Bâteau 3 sélectionné")
                    texte_principal(1) # on change le message pour "placez le vaisseau"
                    affichage_croix_gauche(dico_vaisseaux_placés_en_phase_de_placement2)
            #vaisseau 3 du bas
            if position_souris[0]>=140+500-500//taille*3 and position_souris[0]<=140+500 and position_souris[1]>= 600+500//taille and position_souris[1]<= 640+500//taille:
                if '3b' in vaisseaux_à_placer_j2:
                    nature_action = 1
                    vaisseau_selectionné = '3b'
#                    print("Bâteau 3b sélectionné")
                    texte_principal(1) # on change le message pour "placez le vaisseau"
                    affichage_croix_gauche(dico_vaisseaux_placés_en_phase_de_placement2)
            #vaisseau 2
            if position_souris[0]>=140+500//taille*4+500//(taille*2) and position_souris[0]<=140+500//taille*4+500//(taille*2)+500//taille*2 and position_souris[1]>= 600+500//taille and position_souris[1]<= 640+500//taille:
                if 2 in vaisseaux_à_placer_j2: 
                    nature_action = 1
                    vaisseau_selectionné = 2
#                    print("Bâteau 2 sélectionné")
                    texte_principal(1) # on change le message pour "placez le vaisseau"
                    affichage_croix_gauche(dico_vaisseaux_placés_en_phase_de_placement2)
        if nature_action == 1: # on place ou fait tourner le bâteau
            if position_souris[0]>=150 and position_souris[0]<=650 and position_souris[1]>=70 and position_souris[1]<=570:#on est dans la grille
                ligne = (position_souris[1]-70)//50
                colonne = (position_souris[0]-150)//50
                if vaisseau_selectionné in vaisseaux_à_placer_j2:
                    v=creer_vaisseau_dans_grille(vaisseau_selectionné, (ligne, colonne), direction, grille_position2,dico_vaisseaux_placés_en_phase_de_placement2)
                    if v:
                        dico_vaisseaux2[vaisseau_selectionné]=v
                        direction = "d"
                        nature_action = 0
                        vaisseaux_à_placer_j2.remove(vaisseau_selectionné)
#                        print (dico_vaisseaux2)
                        texte_principal(0)
                        affichage_croix_gauche(dico_vaisseaux_placés_en_phase_de_placement2)
            if position_souris[0]>=690 and position_souris[0]<=690+150 and  position_souris[1]>=580 and position_souris[1]<=580+150:
#                print('rotation', direction)
                if direction == "b":
                    direction ="d"
                    texte_principal(6)
                else:
                    direction = "b"
                    texte_principal(7)
            for image_vaisseaux in dico_vaisseaux2:
                affichage_vaisseaux_dans_grille(dico_vaisseaux2[image_vaisseaux])
        if nature_action == 2: #phase d'attaque!
            if position_souris[0]>=1350-500//taille*(taille-1) and position_souris[0]<=1350-500//taille*(taille-1)+500//taille*taille and position_souris[1]>=70 and position_souris[1]<=570:#on est dans la grille de droite
                ligne = (position_souris[1]-70)//50
                colonne = (position_souris[0]-(1350-500//taille*(taille-1)))//50
                v=attaque_de_la_case((ligne,colonne),grille_position1, dico_vaisseaux1,grille_attaque2)
#                print("v, ",v)
                if v == "vaisseau détruit":
                    menu = 4.5
                    Rafraîchissement_ecran()
                    for image_vaisseaux in dico_vaisseaux2:
                        affichage_vaisseaux_dans_grille(dico_vaisseaux2[image_vaisseaux])
                    affiche_coups_droite(grille_attaque2)
                    affiche_coups_gauche(grille_attaque1)
                    texte_principal(3)
                elif v == "raté":
                    menu = 4.5
                    Rafraîchissement_ecran()
                    for image_vaisseaux in dico_vaisseaux1:
                        affichage_vaisseaux_dans_grille(dico_vaisseaux2[image_vaisseaux])
                    affiche_coups_droite(grille_attaque2)
                    affiche_coups_gauche(grille_attaque1)
                    texte_principal(4)
                elif v == "touché":
                    menu = 4.5      
                    Rafraîchissement_ecran()
                    for image_vaisseaux in dico_vaisseaux1:
                        affichage_vaisseaux_dans_grille(dico_vaisseaux2[image_vaisseaux])
                    affiche_coups_droite(grille_attaque2)
                    affiche_coups_gauche(grille_attaque1)
                    texte_principal(5)
                elif v:
                    menu = 4.5
                    Rafraîchissement_ecran()
                    for image_vaisseaux in dico_vaisseaux2:
                        affichage_vaisseaux_dans_grille(dico_vaisseaux2[image_vaisseaux])
                    affiche_coups_droite(grille_attaque2)
                    affiche_coups_gauche(grille_attaque1)
                    

        if nature_action == 0 and vaisseaux_à_placer_j2==[]:
            menu = 4.5
            Rafraîchissement_ecran()
            for image_vaisseaux in dico_vaisseaux2:
                affichage_vaisseaux_dans_grille(dico_vaisseaux2[image_vaisseaux])
            affiche_coups_droite(grille_attaque2)
            affiche_coups_gauche(grille_attaque1)
            nature_action = 2


    elif menu == 3:
    #menu de transition après une action du premier joueur, avant une action du second
        menu = 2
        
        Rafraîchissement_ecran()
        if nature_action == 0:
            texte_principal(0)
        else:
            texte_principal(2)
#        print(menu)
#        print(nature_action)
        for image_vaisseaux in dico_vaisseaux2:
            affichage_vaisseaux_dans_grille(dico_vaisseaux2[image_vaisseaux])
        affiche_coups_gauche(grille_attaque1)
        affiche_coups_droite(grille_attaque2)

    elif menu == 4:
    #menu de transition après une action du second joueur, avant une action du premier
        menu = 1
        Rafraîchissement_ecran()
        texte_principal(2)
#        print(menu, nature_action)
        for image_vaisseaux in dico_vaisseaux1:
            affichage_vaisseaux_dans_grille(dico_vaisseaux1[image_vaisseaux])
        affiche_coups_droite(grille_attaque1)
        affiche_coups_gauche(grille_attaque2)

    elif menu == 3.5:
    #menu permettant d'attendre un clic de plus avant le menu 3. Cela permet au joueur 1 de voir visuellement ce qu'il vient de faire
        if nature_action == 0:
            affichage_croix_gauche(dico_vaisseaux_placés_en_phase_de_placement1)
        if gagnant !=-1:
            menu = 5
#            print(menu, "partie finie")
            ecran_de_fin()
        else:
            menu = 3
            ecran_passage_autre_joueur()

    elif menu == 4.5:
    #menu permettant d'attendre un clic de plus avant le menu 4. Cela permet au joueur 2 de voir visuellement ce qu'il vient de faire
        if gagnant !=-1:
            menu = 5
            ecran_de_fin()
        else:
            menu = 4
            ecran_passage_autre_joueur()

    elif menu == 5:
    #menu de fin de partie, lorsqu'un joueur a gagné
        restart()

    elif menu == 11:
    #menu du premier joueur en mode contre l'IA
        if nature_action == 0: # on sélectionne les vaisseaux
            #vaisseau 5
            if position_souris[0]>=140 and position_souris[0]<=125+500//taille*5 and position_souris[1]>= 600 and position_souris[1]<= 640:
                if 5 in vaisseaux_à_placer_j1:
                    nature_action = 1
                    vaisseau_selectionné = 5
#                    print("Bâteau 5 sélectionné")
                    texte_principal(1) # on change le message pour "placez le vaisseau"
                    affichage_croix_gauche(dico_vaisseaux_placés_en_phase_de_placement1)
            #vaisseau 4
            if position_souris[0]>=140 and position_souris[0]<=350 and position_souris[1]>= 600+500//taille and position_souris[1]<= 640+500//taille:
                if 4 in vaisseaux_à_placer_j1:    
                    nature_action = 1
                    vaisseau_selectionné = 4
#                    print("Bâteau 4 sélectionné")
                    texte_principal(1) # on change le message pour "placez le vaisseau"
                    affichage_croix_gauche(dico_vaisseaux_placés_en_phase_de_placement1)
            #vaisseau 3 du haut
            if position_souris[0]>=140+500-500//taille*3 and position_souris[0]<=140+500 and position_souris[1]>= 600 and position_souris[1]<= 640:
                if 3 in vaisseaux_à_placer_j1:
                    nature_action = 1
                    vaisseau_selectionné = 3
#                    print("Bâteau 3 sélectionné")
                    texte_principal(1) # on change le message pour "placez le vaisseau"
                    affichage_croix_gauche(dico_vaisseaux_placés_en_phase_de_placement1)
            #vaisseau 3 du bas
            if position_souris[0]>=140+500-500//taille*3 and position_souris[0]<=140+500 and position_souris[1]>= 600+500//taille and position_souris[1]<= 640+500//taille:
                if '3b' in vaisseaux_à_placer_j1:
                    nature_action = 1
                    vaisseau_selectionné = '3b'
#                    print("Bâteau 3b sélectionné")
                    texte_principal(1) # on change le message pour "placez le vaisseau"
                    affichage_croix_gauche(dico_vaisseaux_placés_en_phase_de_placement1)
            #vaisseau 2
            if position_souris[0]>=140+500//taille*4+500//(taille*2) and position_souris[0]<=140+500//taille*4+500//(taille*2)+500//taille*2 and position_souris[1]>= 600+500//taille and position_souris[1]<= 640+500//taille:
                if 2 in vaisseaux_à_placer_j1:
                    nature_action = 1
                    vaisseau_selectionné = 2
#                    print("Bâteau 2 sélectionné")
                    texte_principal(1) # on change le message pour "placez le vaisseau"
                    affichage_croix_gauche(dico_vaisseaux_placés_en_phase_de_placement1)
        if nature_action == 1: # on place ou fait tourner le bâteau
            if position_souris[0]>=150 and position_souris[0]<=650 and position_souris[1]>=70 and position_souris[1]<=570:#on est dans la grille
                ligne = (position_souris[1]-70)//50
                colonne = (position_souris[0]-150)//50
                if vaisseau_selectionné in vaisseaux_à_placer_j1:
                    v=creer_vaisseau_dans_grille(vaisseau_selectionné, (ligne, colonne), direction, grille_position1,dico_vaisseaux_placés_en_phase_de_placement1)
                    if v:
                        dico_vaisseaux1[vaisseau_selectionné]=v
                        direction = "d"
                        nature_action = 0
                        vaisseaux_à_placer_j1.remove(vaisseau_selectionné)
#                        print (dico_vaisseaux1)
                        texte_principal(0)
                        affichage_croix_gauche(dico_vaisseaux_placés_en_phase_de_placement1)
            if position_souris[0]>=690 and position_souris[0]<=690+150 and  position_souris[1]>=580 and position_souris[1]<=580+150:
#                print('rotation', direction)
                if direction == "b":
                    direction ="d"
                    texte_principal(6)
                else:
                    direction = "b"
                    texte_principal(7)
        for image_vaisseaux in dico_vaisseaux1:
            affichage_vaisseaux_dans_grille(dico_vaisseaux1[image_vaisseaux])
        if nature_action == 2: #phase d'attaque!
            if position_souris[0]>=1350-500//taille*(taille-1) and position_souris[0]<=1350-500//taille*(taille-1)+500//taille*taille and position_souris[1]>=70 and position_souris[1]<=570:#on est dans la grille de droite
                ligne = (position_souris[1]-70)//50
#                print("je suis dans la grille de droite")
                colonne = (position_souris[0]-(1350-500//taille*(taille-1)))//50
                v=attaque_de_la_case((ligne,colonne),grille_position2, dico_vaisseaux2,grille_attaque1)
#                print("v, ",v)
                if v == "vaisseau détruit":
                    menu = 12
                    Rafraîchissement_ecran()
                    for image_vaisseaux in dico_vaisseaux1:
                        affichage_vaisseaux_dans_grille(dico_vaisseaux1[image_vaisseaux])
                    affiche_coups_droite(grille_attaque1)
                    affiche_coups_gauche(grille_attaque2)
                    texte_principal(3)
                elif v == "raté":
                    menu = 12
                    Rafraîchissement_ecran()
                    for image_vaisseaux in dico_vaisseaux1:
                        affichage_vaisseaux_dans_grille(dico_vaisseaux1[image_vaisseaux])
                    affiche_coups_droite(grille_attaque1)
                    affiche_coups_gauche(grille_attaque2)
                    texte_principal(4)
                elif v == "touché":
                    menu = 12    
                    Rafraîchissement_ecran()
                    for image_vaisseaux in dico_vaisseaux1:
                        affichage_vaisseaux_dans_grille(dico_vaisseaux1[image_vaisseaux])
                    affiche_coups_droite(grille_attaque1)
                    affiche_coups_gauche(grille_attaque2)
                    texte_principal(5)
                elif v:
                    menu = 12
                    Rafraîchissement_ecran()
                    for image_vaisseaux in dico_vaisseaux1:
                        affichage_vaisseaux_dans_grille(dico_vaisseaux1[image_vaisseaux])
                    affiche_coups_droite(grille_attaque1)
                    affiche_coups_gauche(grille_attaque2)

        if nature_action == 0 and vaisseaux_à_placer_j1==[]:
            menu = 12
            Rafraîchissement_ecran()
            for image_vaisseaux in dico_vaisseaux1:
                affichage_vaisseaux_dans_grille(dico_vaisseaux1[image_vaisseaux])
            affiche_coups_droite(grille_attaque1)
            affiche_coups_gauche(grille_attaque2)
            nature_action = 10
        if gagnant != -1:
            menu = 5
            ecran_de_fin()

    elif menu == 12:
    #menu du tour de l'IA      
        if nature_action == 10:
            grille_position2, dico_vaisseaux2 = ia_placement_choice_pour_affichage_graphique(vaisseaux_à_placer_j2, grille_position2) 
            nature_action = 2
            menu = 11
        elif nature_action == 2:     
            if etat == 0:
                (x, y) = choix_tir_ia_aleatoire(cases_restantes_ia)
            if etat == 1:
                (x, y) = choix_tir_ia_locale(cases_restantes_ia, liste_touchee_ia[-1])
            if etat == 2:
                (x, y) = choix_tir_ligne_ia(grille_attaque2, liste_touchee_ia)
            if etat == 3:
                (x, y) = choix_tir_colonne_ia(grille_attaque2, liste_touchee_ia)
            v = attaque_de_la_case( (x,y), grille_position1, dico_vaisseaux1,grille_attaque2)
            if v == "vaisseau détruit":
                menu = 11
                etat = 0
                liste_touchee_ia.append((x,y))
            elif v == "touché":
                menu = 11
                liste_touchee_ia.append((x,y))
                if etat == 0:
                    etat = 1
                elif etat == 1:  
                    if liste_touchee_ia[-1][0] == liste_touchee_ia[-2][0]:
                        etat = 2
                    if liste_touchee_ia[-1][1] == liste_touchee_ia[-2][1]:
                        etat = 3
            else:
                menu = 11
        affiche_coups_gauche(grille_attaque2)
        if gagnant != -1:
            menu = 5
            ecran_de_fin()
        
    pygame.display.flip()

#Rafraîchissement_ecran()
#tests
# affichage_vaisseaux_dans_grille([5,[(1,2),(1,3)]])
# grille_test=[[0 for j in range (10)] for i in range (10)]
# grille_test[1][8]=2
# grille_test[2][2]=1
# grille_test[1][4]=2
# affiche_coups_gauche(grille_test)

#Cinématique :

cinematic = VideoFileClip("Videos/Cinematic.mp4")
cinematic.preview()

fenetre = pygame.display.set_mode((1530, 900),RESIZABLE)
pygame.mixer.music.load("Sounds/Music.ogg")
pygame.mixer.music.play(loops = -1)

#ecran_passage_autre_joueur()
Menu()
pygame.display.flip()
continuer = 1

#Boucle infinie

while continuer:
    for event in pygame.event.get():
        if event.type == QUIT:
            continuer = 0
        if event.type == MOUSEBUTTONDOWN:
            mouseclick(event.pos)