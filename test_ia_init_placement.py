from ia_init_placement import *
from pytest import * 


def test_possibilites_de_placement():
    my_test_grid1 = [[' ',' ',' '],[' ',1,' '],[' ',' ',' ']]
    my_test_grid2 = [[' ',' ',' '],[' ',' ',2],[' ',' ',' ']]
    # 4 possibilités pour la V0 de la fonction (bateaux voisins autorisés)
    assert (4,True,True,True,True) == (len(possibilités_de_placement(3, my_test_grid1,0)),[[0,0],[0,1],[0,2]] in possibilités_de_placement(3, my_test_grid1,0) , [[2,0],[2,1],[2,2]] in possibilités_de_placement(3, my_test_grid1,0), [[0,0],[1,0],[2,0]] in possibilités_de_placement(3, my_test_grid1,0), [[0,2],[1,2],[2,2]] in possibilités_de_placement(3, my_test_grid1,0))

    # 0 possibilités pour la V1 de la fonction car pas de bateaux voisins autorisés
    assert possibilités_de_placement(3,my_test_grid1) == []

    # Seule la première colonne est autorisée avec la V1
    assert possibilités_de_placement(3, my_test_grid2) == [  [ [0,0],[1,0],[2,0] ] ]

print(test_possibilites_de_placement())



def test_ia_placement_choice():
    navires = [5,4,3,2]
    dir = [ (1,0), (0,1) ]

    def existe_une_liste_de_taille(taille,grille):
        existe = False
        n = len(grille)
        for i in range(n):
            for j in range(n):
                if grille[i][j] != ' ':
                    for d in dir:
                        val = True
                        k=0
                        while val and k<taille-1:
                            x, xprime = i + k * d[0], i + (k+1) * d[0]
                            y, yprime = j + k * d[1], j + (k+1) * d[1]
                            if x>=0 and x<n and y>=0 and y<n and xprime>=0 and xprime<n and yprime>=0 and yprime<n:
                                if grille[x][y] != grille[xprime][yprime]:
                                    val = False
                                k+=1
                        if val == True:
                            return True
        return False
    assert existe_une_liste_de_taille(5,ia_placement_choice(navires)) == True
    assert existe_une_liste_de_taille(4,ia_placement_choice(navires)) == True
    assert existe_une_liste_de_taille(3,ia_placement_choice(navires)) == True
    assert existe_une_liste_de_taille(2,ia_placement_choice(navires)) == True

#print(test_ia_placement_choice())    

