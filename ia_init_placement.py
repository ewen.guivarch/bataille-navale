import random as rd
### Ce fichier comprend 2 grosses fonctions et des fonctions annexes utilitaires. La principale est ia_placement_choice.
# La fonction possibilités_de_placement est la plus complexe, et sert de base à ia_placement_choice.


# Pour chaque taille de vaisseau et pour une grille donnée, "possibilités_de_placement" renvoie toutes les listes de positions éligibles pour placer un tel vaisseau
# Le programme centralise les versions de 0 à 3. Chaque version représente un type de stratégie de placement des vaisseaux.
# Version 0 : Placement aléatoire (tant que la position est possible)
# Version 1 : Placement aléatoire, mais 2 vaisseaux ne peuvent jamais avoir de cases adjacentes (pas de dommages collatéraux )
# Version 2 : La moitié des vaisseaux est placée aléatoirement. L'autre moitié est placée sur des cases diagonalement
#               adjacentes à un vaisseau déjà placé. 
# Version 3 : Pour faire de la diversité du jeu, la V3 nous place pour chaque vaisseau de façon aléatoire en mode V0 (10%), V1 (30%) ou V2 (60%)

def possibilités_de_placement(taille_vaisseau, grille_placement, indice_vaisseau = 0, position_vaisseau_precedent = 2, version = 1):
    
    # En Version 3, on se place aléatoirement dans l'un des modes précédents
    if version == 3: 
        the_strategy_choice = rd.randrange(0,9)
        if the_strategy_choice in [1,2,3]:
            version = 1
            print("1")
        elif the_strategy_choice in [4,5,6,7,8,9]:
            version = 2
            print("2")
        else:
            version = 0
            print("0")


    # Dans la version 2, tous les navires d'indice impair vont être placés avec une extrémité diagonalement adjacente à une extrémité du navire d'indice précédent.
    if version == 2:            
        if indice_vaisseau % 2 == 0:        # Les navires d'indice pair sont placés en mode 1.
            return possibilités_de_placement(taille_vaisseau, grille_placement, version = 1)

        elif indice_vaisseau % 2 == 1:
            extremites = position_vaisseau_precedent[0], position_vaisseau_precedent[-1]    # les 2 extrémités du vaisseau précédent
            dir_vaisseau_abs = [abs(extremites[0][0]-extremites[1][0])//(len(position_vaisseau_precedent)-1), abs(extremites[0][1]-extremites[1][1])//(len(position_vaisseau_precedent)-1)]     # indique si le vaisseau est vertical ou horizontal ( [1,0] ou [0,1])
            possibilites = []               # C'est la liste qui sera renvoyée
            for i in [0,1]:                 # i représente chacune des 2 extrémités
                directions_du_bateau_a_placer = [[1-dir_vaisseau_abs[0],1-dir_vaisseau_abs[1]], [dir_vaisseau_abs[0]-1, dir_vaisseau_abs[1] -1]]        # On ne compte pour l'instant que les 2 directions orthogonales au vaisseau précédent
                dir_vaisseau = [(extremites[i][0]-extremites[1-i][0])//(len(position_vaisseau_precedent)-1), (extremites[i][1]-extremites[1-i][1])//(len(position_vaisseau_precedent)-1)]   #Indique la direction unitaire orientée du vaisseau précédent
                cases_diagonalement_adjacentes_de_cette_extremite = [( extremites[i][0] + directions_du_bateau_a_placer[0][0] + dir_vaisseau[0] , extremites[i][1] + directions_du_bateau_a_placer[0][1] + dir_vaisseau[1] )]       # On calcule l'une des 2 cases diagonalement adjacentes à l'extrémité i, grâce à l'une des 2 directions orthogonales calculées précédemment 
                cases_diagonalement_adjacentes_de_cette_extremite.append( (extremites[i][0] + directions_du_bateau_a_placer[1][0] + dir_vaisseau[0] , extremites[i][1] + directions_du_bateau_a_placer[1][1] + dir_vaisseau[1] ))   # On ajoute la 2ème case diagonalement adjacente (même principe avec la 2ème direction orthogonale 'direction_du_bateau_a_placer' )
                directions_du_bateau_a_placer.append([dir_vaisseau[0],dir_vaisseau[1]])         # Il y a en fait une 3ème direction orientée possible pour le nouveau vaisseau : celle qui est parallèle au vaisseau précédent
                for case in cases_diagonalement_adjacentes_de_cette_extremite:                  # Pour chaque case_diago_adj de chaque extrémité, 
                    for dir in directions_du_bateau_a_placer:                                   # pour chaque direction possible, 
                        la_position_est_éligible = True                                         
                        cette_position = []                                                     # on rajoutera une possibilité si elle vérifie les conditions de base
                        x, y = case[0], case[1]                         # (x,y) est la case_diagonalement_adjacente choisie
                        for k in range(taille_vaisseau):                # k est l'indice linéïque d'avancée dans la longueur du bateau
                            i,j = x + k * dir[0], y + k * dir[1]
                            if not is_in(grille_placement, i, j) or grille_placement[i][j] != ' ':      # vérification des conditions de base
                                la_position_est_éligible = False
                            if voisins(i,j,grille_placement)[0] :                           # voisins(args)[0] est le booléen si tous les voisins sont dans la grille ou non
                                for voisin in voisins(i,j, grille_placement)[1]:            # voisins(args)[1] est la liste des voisins
                                    if grille_placement[voisin[0]][voisin[1]] != ' ':       # Si l'un des voisins est occupé par un vaisseau, position refusée
                                        la_position_est_éligible = False
                            cette_position.append((i,j))
                        if la_position_est_éligible:
                            possibilites.append(cette_position)
            if len(possibilites) == 0:                                  # Si cette stratégie (V2) est trop restrictive et n'offre pas de possibilités, on essaie avec une stratégie moins restrictive
                return possibilités_de_placement(taille_vaisseau, grille_placement)                            

    # Concerne les stratégies V0 et V1
    else:        
        possibilites = []                               # les placements possibles des vaisseaux
        dir = [[1,0],[0,1]]                             # dir[i] multiplié par taille_vaisseau vérifie si les points sont éligibles pour TOUTE la taille du vaisseau
        n= len(grille_placement)
        for i in range(n):
            for j in range(n):                          # On parcourt chaque point (i,j)
                for d in dir:                           # pour chaque point, on regarde les 4 directions
                    position_éligible = []              # toutes les coordonnées de la position de vaisseau éligible
                    la_position_est_éligible = True
                    for k in range(taille_vaisseau):    # k est l'indice linéique d'avancée dans la direction choisie
                        x = i + k * d[0]
                        y = j + k * d[1]
                        pos = [x,y]                     
                        if not is_in(grille_placement, x, y):   # Si on est sorti de la grille, la position est mauvaise
                            la_position_est_éligible = False
                        elif grille_placement[x][y] !=  ' ':                 # Si la case considérée n'est pas vide (contient un vaisseau)
                            la_position_est_éligible = False
                        if version == 1:                            # Dans la version 1, on ne veut pas placer de vaisseaux côte à côte.
                            toutes_les_directions = [ [-1,0],[1,0],[0,-1],[0,1]]
                            for direction_proche in toutes_les_directions:            # On vérifie que toutes les cases voisines sont vides pour que la position soit éligible
                                direction_proche = list(direction_proche)
                                x_voisin , y_voisin = x + direction_proche[0], y + direction_proche[1]
                                if ((x_voisin>=0) and (x_voisin<n) and (y_voisin>=0) and (y_voisin<n) ):  # Le test ne concerne que les cases dans la grille
                                    if grille_placement[x_voisin][y_voisin] != ' ' :        # On élimine la position si l'un des voisins n'est pas vide
                                        la_position_est_éligible = False

                        position_éligible.append(pos)
                # Ici, on a créé, pour chaque point (i,j) et pour chaque direction (g,d,h,b), une position éligible.
                    if (position_éligible not in possibilites) and la_position_est_éligible :
                        possibilites.append(position_éligible)
    return possibilites
# possibilités_de_placement de la forme [   [ [i,j], [i+1,j], ... , ] , [  [x,y], ...,              ]   ]

# La fonction n'est plus utile, mais a servi à construire le code. 
def existe_en_doublon(grille, i,j,dir, taille, possib):
    dir_opposee = [-dir[0],-dir[1]]
    test_de_doublon = []                # Liste qui contiendra la liste possiblement déjà existante dans "possib"
    x = i + (taille-1) * dir[0]         # On revient avec (x,y) au début de la liste possiblement déjà existante (pour l'ordre dans la liste)
    y = j + (taille-1) * dir[1]
    for k in range(taille):             # On parcourt la liste dans le sens opposé et on remplit une liste possiblement déjà existante
        test_de_doublon.append([x + k * dir_opposee[0],y+k*dir_opposee[1]])
    if test_de_doublon in possib:       # Test final si la liste est déjà répertoriée
        return True
    else :
        return False

# Renvoie le booléen de si (x,y) appartient à la grille.
def is_in(grid, x, y):
    n=len(grid)
    return x>=0 and x<n and y>=0 and y<n

# Renvoie (True, Liste_des_coordonées_des_voisins), ou False si l'un des voisins est hors de la grille.
def voisins(i,j, grille):
    directions = [ [-1,0],[1,0],[0,-1],[0,1]]
    vois = []
    for dir in directions:
        x,y = i + dir[0], j + dir[1]
        if is_in(grille, x,y):
            vois.append([x,y])
        else:
            return (False,)
    return (True, vois)
        
# Placement automatique des vaisseaux par l'IA dans sa grille vide et utilisé pour affichage console.
# Entrée : navires_a_placer : LIST [taille_vaisseau1, taille_vaisseau2, ...]
# RETURN : ( grille avec taille_du_vaisseau sur les cases occupées  ,  liste_des_vaisseaux (voir ci-dessous)  ,   [INTEGER_nombre_de_vaisseaux])
def ia_placement_choice(navires_a_placer, version = 3):
    points_occupes_par_le_vaisseau = []
    taille_grille = 10
    vaisseaux = {}   # vaisseaux (=dico) contiendra pour chaque vaisseau une liste de la forme [taille_vaiss , liste_coordonnées, [] (pour les touchés), nom_vaisseau]
    nombre_de_vaisseaux = len(navires_a_placer)

     # création de ma grille vide (remplie de ' ')
    grille_placement = [[' ' for i in range(taille_grille)] for j in range(taille_grille)]

    # On parcourt chaque vaisseau, identifié par son indice_vaisseau (position dans la liste originelle)
    for indice_vaisseau in range(nombre_de_vaisseaux):
        if navires_a_placer[indice_vaisseau] == '3b':
            taille_du_vaisseau = 3
        else:
            taille_du_vaisseau = navires_a_placer[indice_vaisseau]
        # On choisit maintenant une des positions proposées par la fonction possibilités_de_placement.
        choix_de_position = rd.choice(possibilités_de_placement(taille_du_vaisseau, grille_placement, indice_vaisseau, points_occupes_par_le_vaisseau, version))
        points_occupes_par_le_vaisseau = []

        for point in choix_de_position:                                    # point est une liste [i,j] des points du bateau
            i= point[0]
            j=point[1]
            grille_placement[i][j] = str(navires_a_placer[indice_vaisseau])     # Modification de la grille principale
            points_occupes_par_le_vaisseau.append((i,j))
        vaisseaux[navires_a_placer[indice_vaisseau]] = [taille_du_vaisseau, points_occupes_par_le_vaisseau, [], navires_a_placer[indice_vaisseau]]
    return (grille_placement, vaisseaux, [nombre_de_vaisseaux])

# Pour l'affichage graphique commun
# On rajoute en argument une grille grille_placement (qu'on ne crée plus dans la fonction), et qui est modifiée dans le programme.
# On ne renvoie que le dictionnaire "vaisseaux"
def ia_placement_choice_pour_affichage_graphique(navires_a_placer, grille_placement, version = 3):
    points_occupes_par_le_vaisseau = []
    taille_grille = 10
    vaisseaux = {}   # vaisseaux (=dico) contiendra pour chaque vaisseau une liste de la forme [taille_v , liste_coordonnées, [] (pour les touchés), nom_vaisseau]
    nombre_de_vaisseaux = len(navires_a_placer)

     # création de ma grille vide (remplie de ' ')
    grille_placement = [[' ' for i in range(taille_grille)] for j in range(taille_grille)]

    # On parcourt chaque vaisseau, identifié par son indice_vaisseau (position dans la liste originelle)
    for indice_vaisseau in range(nombre_de_vaisseaux):
        if navires_a_placer[indice_vaisseau] == '3b':
            taille_du_vaisseau = 3
        else:
            taille_du_vaisseau = navires_a_placer[indice_vaisseau]
        # On choisit maintenant une des positions proposées par la fonction possibilités_de_placement.
        choix_de_position = rd.choice(possibilités_de_placement(taille_du_vaisseau, grille_placement, indice_vaisseau, points_occupes_par_le_vaisseau, version))
        points_occupes_par_le_vaisseau = []

        for point in choix_de_position:            # point est une liste [i,j] des points du bateau
            i= point[0]
            j=point[1]
            grille_placement[i][j] = str(navires_a_placer[indice_vaisseau])
            points_occupes_par_le_vaisseau.append((i,j))
        vaisseaux[navires_a_placer[indice_vaisseau]] = [taille_du_vaisseau, points_occupes_par_le_vaisseau, [], navires_a_placer[indice_vaisseau]]
    #print(grille_placement)
    return (grille_placement, vaisseaux)

# Fonctions d'affichage console pour tester visuellement la fonction.
def grid_to_string(game_grid):
    n = len(game_grid)
    m= max_numbers(game_grid)
    horizontal_delimiter = " " + m*"=" + " " + (n-2)*(m*"=" + " ") + m*"=" + " " +"\n"
    my_string = horizontal_delimiter
    for i in range(n):
        my_string = my_string + full_a_line_bis(game_grid, i) + "\n" + horizontal_delimiter
    return(my_string)
def max_numbers(game_grid):
    n = len(game_grid)
    max=3
    for i in range(n):
        for j in range(n):
            if len(str(game_grid[i][j])) > max :
                max = len(str(game_grid[i][j]))
    return max
def full_a_line_bis(game_grid, i):
    n = len(game_grid)
    new_line="|"
    modules=[]
    m = max_numbers(game_grid)
    for j in range(n):
        modules.append(str(game_grid[i][j]).center(m))
    for module in modules:
        new_line = new_line + module + "|"
    return(new_line)

#my_armada = [5,4,3,'3b',2]
#print(grid_to_string(ia_placement_choice(my_armada, 3)[0]))

