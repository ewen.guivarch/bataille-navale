# Bataille Spatiale

Ligne 6 : Français
Line 43 : English

---------------------------------------------------------------------------------------------------------------------
>>>PRINCIPE DU JEU : Bataille Spatiale
Le jeu a les mêmes règles que la bataille navale, les vaisseaux spatiaux correspondent aux bateaux et ont la même forme.
Il y a deux modes de jeu : un mode solo (contre une IA) et un mode à deux joueurs qui jouent successivement.

Le fonctionnement est assez intuitif. A l'initialisation, le joueur clique sur le bateau qu'il veut placer,
peut le faire pivoter de 90 degrés en appuyant sur le bouton correspondant. Pour choisir la position, il clique sur la 
case la plus à gauche ou la plus en haut qu'il veut occuper.
Au cours du jeu, le joueur voit deux grilles affichées. A gauche il voit sa grille de placement, et à droite la grille ennemie.
A chaque tour, il peut attaquer une case adverse en cliquant dessus.

La partie s'arrête quand un joueur voit ses 5 vaisseaux spatiaux détruits.

>>>PRÉ-REQUIS
Les modules suivant sont nécessaires pour le jeu. (À installer avec 'pip install')
- pygame
- moviepy
- random

>>>INSTALLATION
Ouvrez le jeu avec Visual Studio Code. Bien vérifier que le chemin de la console aboutit au dossier '\bataille-navale' sinon le programme ne trouvera pas les images. Lancez le fichier main_game.py. Découvrez une magnifique cinématique, puis jouez !


>>>POSSIBILITÉS D'INNOVATION
Avoir un menu de paramètres (taille de la grille, configuration des vaisseaux, activation ou non des événements, langue).
Types de bombe (bombe à fragmentation)
Usage de la force : délimite une zone qui contient un vaisseau.
Événements aléatoires (tempête,  étoile de la mort...)
Différentes maps avec des vraies formes.
Campagne scénarisée
Pouvoir déplacer un vaisseau au lieu de tirer. Cases inoccupables à cause d'un astéroïde.


>>>AUTEURS:
Ewen Guivarc’h
Jolan Tissier
Rudy Jacob
Thomas Notz
Emile Prost
Romain Bellon


----------------------------------------------------------------------------------------------------------
>>>GAME PRINCIPLE : Spatial battle
The rules are the same as in naval battle, spaceships correspond to boats and have the same shape.
There are two game modes : a solo mode (against an AI) and a two players mode where they play one after the other.

The way of playing is rather intuitive. When initiating, the player clicks on the boat he wants to place,
can rotate it of 90 degrees by clicking on the corresponding button. To choose position, he clicks on the cell that will 
correspond to the leftest or the uppest of the ship. 
During game, the player sees two grids. On the left, he can see his placement grid, and on the right his enemy's grid.
Every turn, he can attack an adversary cell by clicking on it.

The game ends when a player loses his 5 spaceships.

>>>REQUIREMENT
You need to install the following modules -with 'pip install'-:
- pygame
- moviepy
- random

>>>INSTALLATION
Open the game with Visual Studio Code. Verify that terminal path end in the file '\bataille-navale', else the program will not achieve to load the images. Run main_game.py. Discover a beautiful cinematic, and then play !

>>>INNOVATION POSSIBILITIES
Having a menu for parameters (grid size, spaceships configuration, activating or not the events, language).
Type of bomb (fragmentation bomb).
Using the Force : show an area that contains a spaceship.
More random events (storm, Death Star...)
Different maps with real shapes.
Scenarized campaign.
Allowing to move a spaceship instead of shooting. Inaccessible cells due to the presence of an asteroid.

>>>AUTHORS:
Ewen Guivarc'h
Jolan Tissier
Rudy Jacob
Thomas Notz
Emile Prost
Romain Bellon