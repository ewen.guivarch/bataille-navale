import pygame
from pygame.locals import *

pygame.init()
fenetre = pygame.display.set_mode((300,300))
'''Au début du code'''
pygame.mixer.music.load("Sounds/Music.ogg")
tirLaser1 = pygame.mixer.Sound("Sounds/tirLaser1.wav")
tirLaser2 = pygame.mixer.Sound("Sounds/tirLaser2.wav")
explosion = pygame.mixer.Sound("Sounds/vaisseauBoum.wav")
bipbipbip = pygame.mixer.Sound("Sounds/bipbipbip.wav")
hyperespace = pygame.mixer.Sound("Sounds/Hyperespace.wav")
#pygame.mixer.music.play(loops = -1)

'''Tir Laser 1'''
tirLaser1.play()


'''Tir Laser 2'''
#tirLaser2.play()
#tirLaser2.stop()

'''Explosion d'un vaisseau'''
#explosion.play()
#explosion.stop()

'''Bip Bip Bip électronique : '''
#bipbipbip.play()
#bipbipbip.stop()

"""VIDEO"""
'''Au début'''
from moviepy.editor import *
'''A l'endroit'''
cinematic = VideoFileClip("Videos/Cinematic.mp4")
cinematic.preview()

continuer = True
while continuer :
    for event in pygame.event.get() :
        if event.type == QUIT:
            continuer = 0