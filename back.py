typesvaisseaux = { 1 : "Chasseur TIE", 2 : "X-Wing", 3 : "Corvette corélienne", 4 : "Croiseur Mon Calamari", 5 : "Destroyer Stellaire", 6 : "SuperDestroyer"}
lettreColonne = {'A' : 0, 'B' : 1, 'C' : 2, 'D' : 3, 'E' : 4, 'F' : 5, 'G' : 6, 'H' : 7, 'I' : 8, 'J' : 9, 'a' : 0, 'b' : 1, 'c' : 2, 'd' : 3, 'e' : 4, 'f' : 5, 'g' : 6, 'h' : 7, 'i' : 8,'j' : 9}

''' 
Représentation de grilles du point de vue du développeur :
    grille_attaque contient, pour chaque case :
        - un 0 si la case n'a jamais été attaquée
        - un 1 si elle l'a été et que c'est dans le vide intersidéral
        - un 2 si un vaisseau y a été touché
        - un 3 si un vaisseau y a été intégralement détruit.
   grille_placement contient, pour chaque case :
        - rien là où il n'y a pas de vaisseau : ' '
        - le numéro i si une case du i-ème vaisseau s'y trouve
'''


def creer_vaisseau(n,case,dir,grille) :
    ''' Créé un vaisseau de n cases, de case de départ case (tuple), et de direction dir ("b" pour bas, "d" pour droite).
    Renvoie False si on ne peut pas mettre le vaisseau'''
    x,y = case
    if dir == "b" and x+n <= 10 : # Ne sortons pas de la grille
        cases = [] #Liste des cases occupées par le vaisseau
        for i in range(n) :
            if grille[x+i][y] == " " : #Si aucun vaisseau n'occupe déjà cette case
                cases.append((x+i,y))
            else : return(False)
    elif dir == "d" and y+n <= 10 : # Ne sortons pas de la grille
        cases = [] #Liste des cases occupées par le vaisseau
        for i in range(n) :
            if grille[x][y+i] == " " : #Si aucun vaisseau n'occupe déjà cette case
                cases.append((x,y+i))
            else : return(False)
    else : # Le placement est incorrect.
        return False
    return([n,cases,[]])

def demande_ligne():
    try :
        x = int(input("numero ligne :")) - 1 #L'utilisateur rentre des données de 1 à n, nous les manipulons de 0 à n-1
        return x
    except(ValueError) :
        return demande_ligne()

def demande_colonne():
    try :
        y = lettreColonne[input("lettre colonne :")] #La colonne est donnée par une lettre
        return y
    except(ValueError):
        return demande_colonne()


def initJoueur(list_bat) :
    '''Initialise la grille et la liste des vaisseaux d'un joueur. list_bat contient la liste du nombre de cases des vaisseaux'''
    grille = [[" " for i in range(10)] for j in range(10)]
    nombrevaisseaux = len(list_bat)
    vaisseaux = [] #Liste des vaisseaux
    for k in range(nombrevaisseaux) :
        i = list_bat[k]
        vaisseau = False
        while not(vaisseau) : #Tant que la définition du vaisseau échoue
            texte = "Vaisseau de " + str(i) + " cases :"
            print(texte)
            x = demande_ligne()
            y = demande_colonne()
            dir = ""
            while dir != "b" and dir != "d" :
                dir = input("Direction (b : bas, d : droite) :") 
            vaisseau = creer_vaisseau(i,(x,y),dir,grille)
        for case in vaisseau[1] :
            x,y = case
            grille[x][y] = k #On remplit la grille par l'indice du vaisseau
        vaisseaux.append(vaisseau) 
    return(grille,vaisseaux,[nombrevaisseaux])
#initJoueur([4,2,1])

def boumboum(x,y,grille_placement,vaisseaux, grille_attaque,nombrevaisseaux) :
    '''Le joueur attaquant, avec sa grille d'attaque grille_attaque attaque l'autre joueur en (x,y).
    L'attaqué voit les tirs sur grille_placement. Le placement de l'autre joueur est donné par vaisseaux et nombrevaisseaux'''
    if grille_placement[x][y] == " " : # Dans l'eau
        grille_attaque[x][y] = 1 # Notifions qu'un tir a été effectué
    elif grille_attaque[x][y] == ' ' : # Un vaisseau était là, intact, et on n'avait pas encore tiré ici.
        nBat = grille_placement[x][y] # On récupère l'indice du vaisseau
        vaisseau = vaisseaux[nBat] # On récupère le vaisseau correspondant
        vaisseau[0] -= 1 # Le vaisseau a une case intacte en moins
        grille_attaque[x][y] = 2 # On actualise la grille des attaques du joueur attaquant
        vaisseau[2].append((x,y)) # On place la case dans la liste des cases touchées du vaisseau
        if vaisseau[0] == 0 : # Le vaisseau est détruit
            nombrevaisseaux[0] -= 1
            destructionvaisseau(vaisseau,grille_attaque)
        return(True) #Touché

def destructionvaisseau(vaisseau,grille_attaque) :
    ''' Le vaisseau est détruit'''
    for case in vaisseau[2] : # On actualise la grille des attaques du joueur attaquant
        x,y = case
        grille_attaque[x][y] = 3
    taille = len(vaisseau[1])
    typeBat = typesvaisseaux[taille]
    print("Un " + typeBat + " a été détruit !")

def afficherGrille(grille) :
    ligneInt = " =  =  =  =  =  =  =  =  =  = "
    txt = ligneInt + "\n"
    for ligne in grille :
        for val in ligne :
            txt = txt + "|" + str(val) + "|"
        txt = txt + "\n" + ligneInt + "\n"
    print(txt)


import événements as ev

def game(list_bat) :
    print("C'est au tour de l'Amiral de la Rébellion")
    grille_placement1,vaisseaux1,nombreBat1 = initJoueur(list_bat)
    print("C'est au tour du Grand Moff de l'Empire")
    grille_placement2,vaisseaux2,nombreBat2 = initJoueur(list_bat)
    grille_attaque1 = [[' ' for i in range(10)] for j in range(10)]
    grille_attaque2 = [[' ' for i in range(10)] for j in range(10)]
    while nombreBat1[0] != 0 and nombreBat2[0] != 0 :
        print("C'est au tour de l'Amiral de la Rébellion !")
        afficherGrille(grille_placement1)
        afficherGrille(grille_attaque1)
        x = int(input("Ligne d'attaque :")) - 1
        y = lettreColonne[input("Colonne d'attaque :")]
        reussite = boumboum(x,y,grille_placement2,vaisseaux2,grille_attaque1,nombreBat2)
        if reussite :
            print("C'est touché !")
        else :
            print("Raté !")
        
        # Événement : arrivée des renforts
        grille_placement1, grille_attaque2, vaisseaux1 = ev.events(grille_placement1, grille_attaque2, grille_placement2, vaisseaux1)

        print("C'est au tour du Grand Moff de l'Empire !")
        afficherGrille(grille_placement2)
        afficherGrille(grille_attaque2)
        x = int(input("Ligne d'attaque :")) - 1
        y = lettreColonne[input("Colonne d'attaque :")]
        reussite = boumboum(x,y,grille_placement1,vaisseaux1,grille_attaque2,nombreBat1)
        if reussite :
            print("C'est touché !")
        else :
            print("Raté !")
    if nombreBat1[0] == nombreBat2[0] :
        print("Match nul")
    elif nombreBat1[0] == 0 :
        print("L'Empire remporte cette bataille !")
    else :
        print("La Rébellion remporte cette bataille !")


from choix_tir_ia import *
from ia_init_placement import *

def game_joueur_vs_ia(list_bat) :
    print("C'est au tour de l'Amiral de la Rébellion")
    grille_placement1,vaisseaux1,nombreBat1 = initJoueur(list_bat)
    print("C'est au tour du Grand Moff de l'Empire")
    grille_placement2,vaisseaux2,nombreBat2 = ia_placement_choice(list_bat)
    grille_attaque1 = [[' ' for i in range(10)] for j in range(10)]
    grille_attaque2 = [[' ' for i in range(10)] for j in range(10)]
    cases_restantes_ia = [ (i,j) for i in range(10) for j in range(10) ]  #on initialise une liste contenant toutes les cases sur lesquelles l'ia n'a pas encore tiré
    Etat_ia = 0  #l'état définit comment l'ia choisit son tir (cf commentaire début choix_tir_ia)
    liste_touchee_ia = []  #on crée une liste des cases touchées par l'ia, le dernier élément de la liste étant la dernière case touchée
    nbr_vaisseaux_joueur_temp = nombreBat1[0]   #on stocke dans une variable temporaire le nombre de vaisseaux restants, on pourra ainsi détecter la destruction d'un vaisseau en comparant cette valeur au nombre présent de vaisseaux
    while nombreBat1[0] != 0 and nombreBat2[0] != 0 :   #tant que chaque joueur a encore des vaisseaux, la partie continue
        print("C'est au tour de l'Amiral de la Rébellion !")  #c'est le tour du joueur
        afficherGrille(grille_placement1)  #on lui montre ses vaisseaux
        afficherGrille(grille_attaque1)    #et la grille montrant les résultats de ses tirs
        x = int(input("Ligne d'attaque :")) - 1   #il entre les coordonnées de la case où il veut tirer
        y = lettreColonne[input("Colonne d'attaque :")]
        reussite = boumboum(x,y,grille_placement2,vaisseaux2,grille_attaque1,nombreBat2)   #on effectue le tir (cf fonction boumboum)
        if reussite :
            print("C'est touché !")
        else :
            print("Raté !")

        print("C'est au tour du Grand Moff de l'Empire !")  #c'est le tour de l'ia
        if Etat_ia == 0:    #si l'ia est à l'état 0, elle tire aléatoirement sur la carte
            (x,y) = choix_tir_ia_aleatoire(cases_restantes_ia)
        elif Etat_ia == 1:  #l'ia tire autour de la dernière case touchée
            (x,y) = choix_tir_ia_locale(cases_restantes_ia, liste_touchee_ia[-1])
        elif Etat_ia == 2:  #l'ia tire sur la ligne de la dernière case touchée
            (x,y) = choix_tir_ligne_ia(grille_attaque2, liste_touchee_ia)
        else:               #l'ia tire sur la colonne de la dernière case touchée
            (x,y) = choix_tir_colonne_ia(grille_attaque2, liste_touchee_ia)
        reussite = boumboum(x,y,grille_placement1,vaisseaux1,grille_attaque2,nombreBat1)
        if reussite :   #si le tir a réussi, on met à jour l'état et liste_touchee
            liste_touchee_ia.append((x,y))
            print("C'est touché !")
            if Etat_ia == 0:
                Etat_ia = 1
            elif Etat_ia == 1:  #si on a touchée une case depuis l'état 1, les deux dernières cases touchées sont adjacente: on regarde si c'est selon les lignes ou colonnes
                if liste_touchee_ia[-1][0] == liste_touchee_ia[-2][0]:
                    Etat_ia = 2
                if liste_touchee_ia[-1][1] == liste_touchee_ia[-2][1]:
                    Etat_ia = 3
            if nbr_vaisseaux_joueur_temp != nombreBat1[0]:   #on regarde si on a détruit un vaisseau, si c'est le cas on réinitialise l'état à 0
                Etat_ia = 0
                nbr_vaisseaux_joueur_temp = nombreBat1[0]
        else :
            print("Raté !")
    if nombreBat1[0] == nombreBat2[0] :   #on regarde le nombre de vaisseaux restant à la fin pour chaque joueur pour déterminer le vainqueur
        print("Match nul")
    elif nombreBat1[0] == 0 :
        print("L'Empire remporte cette bataille !")
    else :
        print("La Rébellion remporte cette bataille !")

#game_joueur_vs_ia([5,4,3,3,2]) 