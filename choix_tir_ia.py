import random as rd 

liste = [ (i,j) for i in range(10) for j in range(10) ]  #il faut initialiser la liste des cases sur laquelle l'ia peut tirer

'''Etat dans [0, 1, 2, 3]  #on définit un état qui définit la manière dont l'IA choisit le tir: aléatoire, autour d'une case, sur une ligne ou sur une colonne '''

def choix_tir_ia_aleatoire(cases_restantes):
    #on choisit la case où l'ia tire parmi celles où elle n'a pas encore tiré (:= cases_restantes)
    n = len(cases_restantes)
    p = rd.randrange(n)   
    return(cases_restantes.pop(p))  #on retourne une case choisit de manière aléatoire, et on l'enlève des cases restantes

def choix_tir_ia_locale(cases_restantes, derniere_case_touchee):
    #L'ia va prendre en compte le résultat du tir précédent pour choisir son nouveau tir
    i, j = derniere_case_touchee[0], derniere_case_touchee[1]
    case_adjacente = []  #on va chercher les cases autour de la dernière case touchée sur lesquels on n'a pas encore tiré
    if (i-1,j) in cases_restantes:
        case_adjacente.append((i-1,j))
    if (i+1,j) in cases_restantes:
        case_adjacente.append((i+1,j))
    if (i,j-1) in cases_restantes:
        case_adjacente.append((i,j-1))
    if (i,j+1) in cases_restantes:
        case_adjacente.append((i,j+1))
    if case_adjacente != []:            #si on n'a pas tiré sur toutes les cases adjacentes à la dernière touchée, on essaie parmi les cases adjacentes restantes
        case = rd.choice(case_adjacente)
        cases_restantes.remove(case)
        return(case)                               
    return(choix_tir_ia_aleatoire(cases_restantes))   #sinon on essaie de manière aléatoire

def choix_tir_ligne_ia(Grille_attaque, liste_touchee):  #si on a deux touchés sur une ligne, on se concentre sur celle_ci jusqu'à la destruction d'un vaisseau
    ligne = liste_touchee[-1][0]
    g = liste_touchee[-1][1]
    d = g
    cases = []
    while Grille_attaque[ligne][d] == 2 and d < len(Grille_attaque)-1: #en partant de la dernière case touchée, on cherche la première case non-touchée vers la droite
        d = d+1
    while Grille_attaque[ligne][g] == 2 and g > 0:   #idem vers la gauche
        g = g-1
    if Grille_attaque[ligne][g] == ' ':  #si cette case n'a pas encore été visée, on l'ajoute dans les possibilités de tir
        cases.append((ligne,g))
    if Grille_attaque[ligne][d] == ' ':  #si cette case n'a pas encore été visée, on l'ajoute dans les possibilités de tir
        cases.append((ligne,d))
    if cases == []:   #si il n'y a pas de case disponible et que l'on n'a pas détruit de vaisseau (vérifié dans le code par la variable d'état), 
    #on cherche dans une colonne 
        return(choix_tir_colonne_ia(Grille_attaque, liste_touchee))
    tir = rd.choice(cases)   #sinon on choisit la case de tir parmi celles retenues précédemment
    return(tir)

def choix_tir_colonne_ia(Grille_attaque, liste_touchee):  #si on a deux touchés sur une ligne, on se concentre sur celle_ci jusqu'au coulé
    #même code que pour choix_tir_ligne_ia mais pour une colonne
    col = liste_touchee[-1][1]
    i = liste_touchee[-1][0]
    j = i
    cases = []
    while Grille_attaque[j][col] == 2 and j < len(Grille_attaque)-1:
        j = j+1
    while Grille_attaque[i][col] == 2 and i > 0:
        i = i-1
    if Grille_attaque[i][col] == ' ':
        cases.append((i,col))
    if Grille_attaque[j][col] == ' ':
        cases.append((j,col))
    if cases == []:   
        return(choix_tir_ligne_ia(Grille_attaque, liste_touchee))    
    tir = rd.choice(cases)
    return(tir)


